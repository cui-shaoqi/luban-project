/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.order.admin.protocol.query;

import com.tedu.inn.protocol.pager.SimplePagerQuery;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;


@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderQuery extends SimplePagerQuery {

    @ApiModelProperty("订单编号")
    String orderNo;

    @ApiModelProperty("订单状态")
    Integer status;

    @ApiModelProperty("用户姓名")
    String userName;

    @ApiModelProperty("用户手机号")
    String userPhone;

    @ApiModelProperty("师傅姓名")
    String workerName;

    @ApiModelProperty("师傅手机号")
    String workerPhone;

    @ApiModelProperty("开始时间")
    Long start;

    @ApiModelProperty("结束时间")
    Long end;

}