/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.order.admin.infrastructure.persistence.data.converter;

import com.tedu.inn.protocol.dao.StatusCriteria;
import com.tarena.tp.luban.order.admin.bo.OrderLogBO;
import com.tarena.tp.luban.order.po.OrderLog;
import com.tarena.tp.luban.order.admin.protocol.param.OrderLogParam;
import com.tarena.tp.luban.order.admin.protocol.query.OrderLogQuery;
import com.tarena.tp.luban.order.admin.dao.query.OrderLogDBPagerQuery;
import org.springframework.beans.BeanUtils;
import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.sdk.context.SecurityContext;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class OrderLogConverter{

   public OrderLogDBPagerQuery toDbPagerQuery(OrderLogQuery orderLogQuery) {
           if (orderLogQuery == null) {
               return new OrderLogDBPagerQuery();
           }
           OrderLogDBPagerQuery orderLogDBPagerQuery = new OrderLogDBPagerQuery();
           BeanUtils.copyProperties(orderLogQuery, orderLogDBPagerQuery);
           return orderLogDBPagerQuery;
       }
   

     public OrderLog param2po(OrderLogParam param) {
        OrderLog orderLog = new OrderLog();
        BeanUtils.copyProperties(param, orderLog);

        LoginUser loginUser=SecurityContext.getLoginToken();

        orderLog.setGmtCreate(System.currentTimeMillis());
        orderLog.setGmtModified(orderLog.getGmtCreate());
        orderLog.setCreateUserId(loginUser.getUserId());
        orderLog.setModifiedUserId(loginUser.getUserId());
        orderLog.setStatus(1);

        orderLog.setCreateUserName(loginUser.getUserName());
        orderLog.setModifiedUserName(loginUser.getUserName());
        return orderLog;
    }

     public OrderLogBO po2bo(OrderLog orderLog) {
        OrderLogBO orderLogBO = new OrderLogBO();
        BeanUtils.copyProperties(orderLog, orderLogBO);
        return orderLogBO;
    }

     public List<OrderLogBO> poList2BoList(List<OrderLog> list) {
        List<OrderLogBO> orderLogBos = new ArrayList<>(list.size());
        for (OrderLog orderLog : list) {
            orderLogBos.add(this.po2bo(orderLog));
        }
        return orderLogBos;
    }

    public void convertStatus(StatusCriteria statusCriteria){
            LoginUser loginUser = SecurityContext.getLoginToken();
            statusCriteria.setModifiedUserId(loginUser.getUserId());
            statusCriteria.setModifiedUserName(loginUser.getUserName());
            statusCriteria.setGmtModified(System.currentTimeMillis());
    }
}