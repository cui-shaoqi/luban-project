/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.order.admin.infrastructure.persistence.repository;

import com.tarena.tp.luban.order.admin.dao.OrderLogDAO;
import com.tedu.inn.protocol.enums.StatusRecord;
import com.tedu.inn.protocol.dao.StatusCriteria;
import com.tarena.tp.luban.order.admin.infrastructure.persistence.data.converter.OrderLogConverter;
import com.tarena.tp.luban.order.po.OrderLog;
import com.tarena.tp.luban.order.admin.bo.OrderLogBO;
import com.tarena.tp.luban.order.admin.protocol.param.OrderLogParam;
import com.tarena.tp.luban.order.admin.repository.OrderLogRepository;
import com.tarena.tp.luban.order.admin.protocol.query.OrderLogQuery;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderLogRepositoryImpl implements OrderLogRepository {
    @Autowired
    private OrderLogConverter orderLogConverter;

    @Autowired
    private OrderLogDAO orderLogDao;


    @Override public List<OrderLogBO> getOrderLogByOrderNo(String orderNo) {
        List<OrderLog> orderLog = this.orderLogDao.getOrderLogByOrderNo(orderNo);
        return this.orderLogConverter.poList2BoList(orderLog);
    }


}