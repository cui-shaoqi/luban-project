/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.order.admin.infrastructure.persistence.data.converter;

import com.tarena.tp.luban.order.admin.bo.OrderInfoBO;
import com.tarena.tp.luban.order.admin.bo.RequestOrderInfoBO;
import com.tarena.tp.luban.order.admin.bo.UserInfoBO;
import com.tedu.inn.protocol.dao.StatusCriteria;
import com.tarena.tp.luban.order.admin.bo.OrderBO;
import com.tarena.tp.luban.order.po.Order;
import com.tarena.tp.luban.order.admin.protocol.param.OrderParam;
import com.tarena.tp.luban.order.admin.protocol.query.OrderQuery;
import com.tarena.tp.luban.order.admin.dao.query.OrderDBPagerQuery;
import org.springframework.beans.BeanUtils;
import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.sdk.context.SecurityContext;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class OrderConverter{

   public OrderDBPagerQuery toDbPagerQuery(OrderQuery orderQuery) {
           if (orderQuery == null) {
               return new OrderDBPagerQuery();
           }
           OrderDBPagerQuery orderDBPagerQuery = new OrderDBPagerQuery();
           BeanUtils.copyProperties(orderQuery, orderDBPagerQuery);
       System.out.println(orderDBPagerQuery.getLimit());
           return orderDBPagerQuery;
       }
   

     public Order param2po(OrderParam param) {
        Order order = new Order();

        if (param == null) {
            return new Order();
        }

        BeanUtils.copyProperties(param, order);

        LoginUser loginUser=SecurityContext.getLoginToken();

        order.setGmtCreate(System.currentTimeMillis());
        order.setGmtModified(order.getGmtCreate());
        order.setCreateUserId(loginUser.getUserId());
        order.setModifiedUserId(loginUser.getUserId());
        order.setStatus(1);

        order.setCreateUserName(loginUser.getUserName());
        order.setModifiedUserName(loginUser.getUserName());
        return order;
    }

     public OrderBO po2bo(Order order) {
        OrderBO orderBO = new OrderBO();
        if (order == null) {
            return new OrderBO();
        }
        BeanUtils.copyProperties(order, orderBO);
        return orderBO;
    }

     public List<OrderBO> poList2BoList(List<Order> list) {
        List<OrderBO> orderBos = new ArrayList<>(list.size());
        for (Order order : list) {
            orderBos.add(this.po2bo(order));
        }
        return orderBos;
    }

    public void convertStatus(StatusCriteria statusCriteria){
            LoginUser loginUser = SecurityContext.getLoginToken();
            statusCriteria.setModifiedUserId(loginUser.getUserId());
            statusCriteria.setModifiedUserName(loginUser.getUserName());
            statusCriteria.setGmtModified(System.currentTimeMillis());
    }
}