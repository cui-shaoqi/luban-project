package com.tarena.tp.luban.order.admin.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RequestOrderInfoVO {

    @ApiModelProperty("订单号")
    String requestOrderNo;

    @ApiModelProperty("供应商")
    String requestOrderProvider;

    @ApiModelProperty("需求单类型")
    String orderCategoryName;

    @ApiModelProperty("单价")
    Long requestOrderPrice;

    @ApiModelProperty("下单日期")
    Long gmtCreate;

}
