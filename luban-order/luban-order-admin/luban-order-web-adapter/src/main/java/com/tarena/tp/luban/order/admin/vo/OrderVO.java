/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.order.admin.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;


@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderVO {

    @ApiModelProperty("id")
    Long id;

    @ApiModelProperty("订单号")
    String orderNo;

    @ApiModelProperty("用户id")
    Long userId;

    @ApiModelProperty("用户姓名")
    String userName;

    @ApiModelProperty("用户电话")
    String userPhone;

    @ApiModelProperty("用户地址")
    String userAddress;

    @ApiModelProperty("订单号")
    String requestOrderNo;

    @ApiModelProperty("供应商")
    String requestOrderProvider;

    @ApiModelProperty("需求单类型")
    String orderCategoryName;

    @ApiModelProperty("单价")
    Long requestOrderPrice;

    @ApiModelProperty("下单日期")
    Long gmtCreate;

    @ApiModelProperty("状态 10:抢单成功,20:签到成功,30:服务完成待结算,40:完成订单" )
    Integer status;

}