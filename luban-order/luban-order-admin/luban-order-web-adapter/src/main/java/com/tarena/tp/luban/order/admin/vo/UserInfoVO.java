package com.tarena.tp.luban.order.admin.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserInfoVO {

    @ApiModelProperty("用户姓名")
    String userName;

    @ApiModelProperty("用户电话")
    String userPhone;

    @ApiModelProperty("用户地址")
    String userAddress;

}
