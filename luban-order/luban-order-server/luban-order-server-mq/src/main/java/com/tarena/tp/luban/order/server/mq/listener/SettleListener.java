/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tarena.tp.luban.order.server.mq.listener;

import javax.annotation.Resource;

import com.tarena.tp.luban.order.common.enums.OrderStatusEnum;
import com.tarena.tp.luban.order.common.enums.ResultEnum;
import com.tarena.tp.luban.order.server.domain.bo.OrderBO;
import com.tarena.tp.luban.order.server.domain.manager.OrderService;
import com.tarena.tp.luban.order.server.domain.repository.OrderRepository;
import com.tarena.tp.luban.order.server.protocol.dto.OrderMqDTO;
import com.tarena.tp.luban.order.server.protocol.dto.SuccessSettleDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RocketMQMessageListener(topic = "${mq.topic.settle-success-topic:settle-success-topic}", consumerGroup = "${rocketmq.producer.group}")
public class SettleListener implements RocketMQListener<OrderMqDTO> {

    @Resource
    private OrderRepository orderRepository;


    @Autowired
    private OrderService orderService;

    @Override public void onMessage(OrderMqDTO settleDTO) {
        log.info("结算中心成功回调: {}", settleDTO);
        String orderNo = settleDTO.getOrderNo();
        if (StringUtils.isEmpty(orderNo)) {
            log.error("结算回调,订单号为空, message: {}", settleDTO);
            throw new RuntimeException(ResultEnum.SETTLE_ORDER_NO_EMPTY.getMessage());
        }

        OrderBO orderBO = orderRepository.getOrderByOrderNo(orderNo);
        if (null == orderBO) {
            log.error("结算回调,根据订单号查询不到, message: {}", settleDTO);
            throw new RuntimeException(ResultEnum.ORDER_EMPTY.getMessage());
        }

        if (!OrderStatusEnum.ORDER_SERVICE_SUCCESS_UN_SETTLE.getStatus().equals(orderBO.getStatus())) {
            log.error("结算回调,订单状态不正确, message: {}", settleDTO);
            throw new RuntimeException(ResultEnum.SETTLE_CALL_ORDER_STATUS_ERROR.getMessage());
        }
        try {
            orderService.finishOrder(orderNo,settleDTO.getWorkerId());
        } catch (Exception e) {
            log.error("结算回调更新订单状态异常", e);
        }
    }

}
