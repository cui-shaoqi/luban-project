/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.order.server.infrastructure.persistence.repository;

import com.tarena.tp.luban.order.po.OrderLog;
import com.tarena.tp.luban.order.server.dao.OrderLogDAO;
import com.tarena.tp.luban.order.server.domain.bo.OrderLogBO;
import com.tarena.tp.luban.order.server.domain.repository.OrderLogRepository;
import com.tarena.tp.luban.order.server.infrastructure.persistence.data.converter.OrderLogConverter;
import com.tarena.tp.luban.order.server.protocol.param.OrderLogParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class OrderLogRepositoryImpl implements OrderLogRepository {



    @Autowired
    private OrderLogConverter orderLogConverter;

    @Resource
    private OrderLogDAO orderLogDao;

    @Override public Long save(OrderLogParam orderLogParam) {
        OrderLog orderLog = this.orderLogConverter.param2po(orderLogParam);
        if (orderLog.getId() != null) {
            this.orderLogDao.update(orderLog);
            return orderLog.getId();
        }
        this.orderLogDao.insert(orderLog);
        return  orderLog.getId();
    }

    @Override public Integer delete(Long orderLogId) {
        return this.orderLogDao.delete(orderLogId);
    }

    @Override public List<OrderLogBO> getOrderLogByOrderNo(String orderNo) {
        List<OrderLog> orderLog = this.orderLogDao.getOrderLogByOrderNo(orderNo);
        return this.orderLogConverter.poList2BoList(orderLog);
    }


}