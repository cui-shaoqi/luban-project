/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tarena.tp.luban.order.server.infrastructure.service;

import com.tarena.tp.luban.order.server.domain.manager.EventService;
import com.tarena.tp.luban.order.server.protocol.dto.OrderMqDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Slf4j
@Service
public class MqEventServiceImpl implements EventService {

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    @Value("${mq.topic.order-success-topic:order-success-topic}")
    private String orderSettleTopic;

    @Value("${mq.topic.request-order-cancel-topic:request-order-cancel-topic}")
    private String cancelTopic;

    @Override
    public void sendOrderFinishEvent(OrderMqDTO orderMqDTO) {
        rocketMQTemplate.asyncSend(orderSettleTopic, orderMqDTO, new SendCallback() {
            @Override public void onSuccess(SendResult result) {
                log.info("发送mq成功, topic: {}, result:{}", orderSettleTopic, result);
            }
            @Override public void onException(Throwable throwable) {
                //todo 发送失败后续处理
                log.error("发送mq失败, topic: {}, Throwable", orderSettleTopic, throwable);
            }
        });
    }

    @Override
    public void sendOrderCancelEvent(OrderMqDTO orderMqDTO) {
        rocketMQTemplate.asyncSend(cancelTopic, orderMqDTO, new SendCallback() {
            @Override public void onSuccess(SendResult result) {
                log.info("发送mq成功, topic: {}, result:{}", cancelTopic, result);
            }
            @Override public void onException(Throwable throwable) {
                log.error("发送mq失败, topic: {}, Throwable", cancelTopic, throwable);
            }
        });
    }
}
