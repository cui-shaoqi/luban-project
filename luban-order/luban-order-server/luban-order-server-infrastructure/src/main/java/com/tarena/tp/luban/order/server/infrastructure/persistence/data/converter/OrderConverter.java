/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.order.server.infrastructure.persistence.data.converter;

import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.sdk.context.SecurityContext;
import com.tarena.tp.luban.order.po.Order;
import com.tarena.tp.luban.order.server.dao.query.OrderDBPagerQuery;
import com.tarena.tp.luban.order.server.domain.bo.OrderBO;
import com.tarena.tp.luban.order.server.protocol.param.OrderParam;
import com.tarena.tp.luban.order.server.protocol.query.OrderQuery;
import com.tedu.inn.protocol.dao.StatusCriteria;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class OrderConverter{

   public OrderDBPagerQuery toDbPagerQuery(OrderQuery orderQuery) {
           if (orderQuery == null) {
               return new OrderDBPagerQuery();
           }
           OrderDBPagerQuery orderDBPagerQuery = new OrderDBPagerQuery();
           BeanUtils.copyProperties(orderQuery, orderDBPagerQuery);
           return orderDBPagerQuery;
       }
   

     public Order param2po(OrderParam param) {
        Order order = new Order();

        if (param == null) {
            return new Order();
        }

        BeanUtils.copyProperties(param, order);
        LoginUser loginUser = SecurityContext.getLoginToken();
        order.setGmtCreate(System.currentTimeMillis());
        order.setGmtModified(order.getGmtCreate());
        order.setCreateUserId(loginUser != null ? loginUser.getUserId() : 0);
        order.setModifiedUserId(loginUser != null ? loginUser.getUserId() : 0);
        order.setCreateUserName(loginUser != null ? loginUser.getUserName() : "mock");
        order.setModifiedUserName(loginUser != null ? loginUser.getUserName() : "mock");
        return order;
    }

     public OrderBO po2bo(Order order) {
        OrderBO orderBO = new OrderBO();
        if (order == null) {
            return new OrderBO();
        }
        BeanUtils.copyProperties(order, orderBO);
        orderBO.setRequestOrderCategoryName(order.getOrderCategoryName());
        return orderBO;
    }

     public List<OrderBO> poList2BoList(List<Order> list) {
        List<OrderBO> orderBos = new ArrayList<>(list.size());
        for (Order order : list) {
            orderBos.add(this.po2bo(order));
        }
        return orderBos;
    }

    public void convertStatus(StatusCriteria statusCriteria){
            LoginUser loginUser = SecurityContext.getLoginToken();
            statusCriteria.setModifiedUserId(loginUser.getUserId());
            statusCriteria.setModifiedUserName(loginUser.getUserName());
            statusCriteria.setGmtModified(System.currentTimeMillis());
    }
}