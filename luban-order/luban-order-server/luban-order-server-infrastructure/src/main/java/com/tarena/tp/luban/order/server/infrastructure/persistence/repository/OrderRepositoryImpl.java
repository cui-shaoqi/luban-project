/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.order.server.infrastructure.persistence.repository;

import com.tarena.tp.luban.order.po.Order;
import com.tarena.tp.luban.order.server.dao.OrderDAO;
import com.tarena.tp.luban.order.server.domain.bo.OrderBO;
import com.tarena.tp.luban.order.server.domain.repository.OrderRepository;
import com.tarena.tp.luban.order.server.infrastructure.persistence.data.converter.OrderConverter;
import com.tarena.tp.luban.order.server.protocol.param.OrderParam;
import com.tarena.tp.luban.order.server.protocol.query.OrderQuery;
import com.tedu.inn.protocol.dao.StatusCriteria;
import com.tedu.inn.protocol.enums.StatusRecord;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class OrderRepositoryImpl implements OrderRepository {

    @Resource
    private OrderConverter orderConverter;

    @Resource
    private OrderDAO orderDao;

    @Override
    public Long save(OrderParam orderParam) {
        Order order = this.orderConverter.param2po(orderParam);
        if (order.getId() != null) {
            this.orderDao.update(order);
            return order.getId();
        }
        this.orderDao.insert(order);
        return  order.getId();
    }

    @Override public Integer delete(Long orderId) {
        return this.orderDao.delete(orderId);
    }

    @Override public Integer disable(String orderIds) {
        StatusCriteria statusCriteria = new StatusCriteria(orderIds, StatusRecord.DISABLE.getStatus());
        this.orderConverter.convertStatus(statusCriteria);
        return this.orderDao.changeStatus(statusCriteria);
    }

    @Override public Integer enable(String orderIds) {
        StatusCriteria statusCriteria = new StatusCriteria(orderIds, StatusRecord.ENABLE.getStatus());
        this.orderConverter.convertStatus(statusCriteria);
        return this.orderDao.changeStatus(statusCriteria);
    }

    @Override public OrderBO getOrder(Long orderId) {
        Order order = this.orderDao.getEntity(orderId);
        return this.orderConverter.po2bo(order);
    }

    @Override
    public OrderBO getOrderByOrderNo(String orderNo) {
        Order order = this.orderDao.getOrderByOrderNo(orderNo);
        return this.orderConverter.po2bo(order);
    }


    @Override public List<OrderBO> queryOrders(
        OrderQuery orderQuery) {
        List<Order> orderList = this.orderDao.queryOrders(this.orderConverter.toDbPagerQuery(orderQuery));
        return this.orderConverter.poList2BoList(orderList);
    }

    @Override public Long getOrderCount(OrderQuery orderQuery) {
        return this.orderDao.countOrder(this.orderConverter.toDbPagerQuery(orderQuery));
    }
}