package com.tarena.tp.luban.order.server.web.adapter.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderInfoVo {

    @ApiModelProperty("订单号")
    String orderNo;

    @ApiModelProperty("创建日期")
    Long createTime;

    @ApiModelProperty("订单状态")
    Integer status;

    @ApiModelProperty("签到日期")
    Long signTime;

    @ApiModelProperty("完成日期")
    Long finishTime;


}
