/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.order.server.web.adapter.controller;

import com.tarena.tp.luban.order.server.domain.bo.OrderBO;
import com.tarena.tp.luban.order.server.domain.manager.OrderService;
import com.tarena.tp.luban.order.server.web.adapter.assemble.OrderAssemble;
import com.tarena.tp.luban.order.server.web.adapter.vo.OrderDetailVO;
import com.tarena.tp.luban.order.server.web.adapter.vo.OrderVO;
import com.tarena.tp.luban.order.server.protocol.param.OrderConfirmParam;
import com.tarena.tp.luban.order.server.protocol.param.OrderParam;
import com.tarena.tp.luban.order.server.protocol.param.OrderFinishParam;
import com.tarena.tp.luban.order.server.protocol.param.OrderSignParam;
import com.tarena.tp.luban.order.server.protocol.query.OrderQuery;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tedu.inn.protocol.pager.PagerResult;
import io.swagger.annotations.Api;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(value = "order", tags = "订单")
@Slf4j
@Validated
@RestController
@RequestMapping("order")
public class OrderController {

    @Resource
    private OrderService orderService;

    @Resource
    private OrderAssemble orderAssemble;


    @PostMapping(value = "create")
    @ApiOperation("抢单")
    public String create(@RequestBody OrderParam orderCreateParam) throws BusinessException {
        return orderService.create(orderCreateParam);
    }

    @PostMapping(value = "sign")
    @ApiOperation("签到")
    public String sign(@RequestBody OrderSignParam orderSignParam) throws BusinessException {
        return orderService.sign(orderSignParam);
    }

    @PostMapping(value = "confirm")
    @ApiOperation("上传服务图片")
    public String confirm(@RequestBody OrderConfirmParam orderConfirmParam) throws BusinessException  {
        return orderService.confirm(orderConfirmParam);
    }

    @PostMapping(value = "finish")
    @ApiOperation("完成订单")
    @RequestMapping
    public String finish(@RequestBody OrderFinishParam orderFinishParam) throws BusinessException {
        return orderService.finish(orderFinishParam);
    }


    @PostMapping(value = "list")
    @ApiOperation("订单列表")
    public PagerResult<OrderVO> list(@RequestBody OrderQuery orderQuery) throws BusinessException {
        ListRecordTotalBO<OrderBO> list = orderService.list(orderQuery);
        return this.orderAssemble.assemblePagerResult(list, orderQuery);
    }

    @GetMapping(value = "info")
    @ApiOperation("订单详情")
    public OrderDetailVO getOrder(@RequestParam String orderNo) throws BusinessException  {
        //fix a bug
        OrderBO orderBo = orderService.getOrderByOrderNo(orderNo);
        return this.orderAssemble.assembleBO2DetailVO(orderBo);
    }
    
}
