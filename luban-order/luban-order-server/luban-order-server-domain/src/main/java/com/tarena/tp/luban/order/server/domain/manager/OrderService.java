/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.order.server.domain.manager;

import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.sdk.context.SecurityContext;
import com.tarena.tp.attach.server.client.AttachApi;
import com.tarena.tp.attach.server.dto.AttachDTO;
import com.tarena.tp.attach.server.param.AttachUpdateParam;
import com.tarena.tp.attach.server.query.AttachQuery;
import com.tarena.tp.luban.demand.server.client.RequestOrderApi;
import com.tarena.tp.luban.order.common.enums.OrderStatusEnum;
import com.tarena.tp.luban.order.common.enums.ResultEnum;
import com.tarena.tp.luban.order.server.domain.assembler.OrderAssembler;
import com.tarena.tp.luban.order.server.domain.bo.AttachInfoBO;
import com.tarena.tp.luban.order.server.domain.bo.OrderBO;
import com.tarena.tp.luban.order.server.domain.bo.OrderLogBO;
import com.tarena.tp.luban.order.server.domain.repository.OrderLogRepository;
import com.tarena.tp.luban.order.server.domain.repository.OrderRepository;
import com.tarena.tp.luban.order.server.protocol.param.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.tarena.tp.luban.order.server.protocol.query.OrderQuery;
import com.tedu.inn.commons.utils.Asserts;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;

@Slf4j
@Service
public class OrderService {

    @Resource
    private OrderRepository orderRepository;

    @Resource
    private OrderLogRepository orderLogRepository;

    @Resource
    private UidGenerator uidGenerator;

    @Resource
    private OrderAssembler orderAssembler;

    @Autowired
    private AttachApi attachApi;

    @Value("${app.business.type}")
    private Integer businessType;

    @Value("${url}")
    private String url;

    @Resource
    private EventService eventService;

    @Autowired
    private RequestOrderApi requestOrderApi;


    public String sign(OrderSignParam orderSignParam) throws BusinessException {
        try {
            Long userId = getUserIdByToken();
            OrderService orderService = (OrderService) AopContext.currentProxy();
            orderService.doUpdate(orderSignParam.getOrderNo(),userId,OrderStatusEnum.ORDER_SIGN_SUCCESS);
        } catch (BusinessException e) {
            log.error("签到异常", e);
            throw e;
        }
        return orderSignParam.getOrderNo();
    }

    public String finish(OrderFinishParam orderFinishParam) throws BusinessException {
        try {
            Long userId = getUserIdByToken();
            OrderService orderService = (OrderService) AopContext.currentProxy();
            OrderBO orderBO = orderService.doUpdate(orderFinishParam.getOrderNo(), userId,
                    OrderStatusEnum.ORDER_SERVICE_SUCCESS_UN_SETTLE);
            eventService.sendOrderFinishEvent(orderAssembler.assembleOrderMqDTO(orderBO));
        } catch (BusinessException e) {
            log.error("完成订单异常", e);
            throw e;
        }
        return orderFinishParam.getOrderNo();
    }

    public void finishOrder(String orderNo,Long userId) throws BusinessException {
        OrderService orderService = (OrderService) AopContext.currentProxy();
        orderService.doUpdate(orderNo,userId,OrderStatusEnum.ORDER_FINISH);
    }

    public String confirm(OrderConfirmParam orderConfirmParam) throws BusinessException {
        try {
            OrderBO orderBO = orderRepository.getOrderByOrderNo(orderConfirmParam.getOrderNo());
            Asserts.isTrue(orderBO == null,new BusinessException(ResultEnum.ORDER_EMPTY));
            createAttach(orderBO.getId().intValue(),orderConfirmParam.getAttachIds());
        } catch (BusinessException e) {
            log.error("施工图片绑定异常", e);
            throw e;
        }
        return orderConfirmParam.getOrderNo();
    }

    private void createAttach(Integer businessId,List<Long> fileIds) {
        List<AttachUpdateParam> attachUpdateParams = new ArrayList<>();
        // fileIds 文件id 前端入参
        fileIds.forEach(id -> {
            AttachUpdateParam updateParam = new AttachUpdateParam();
            updateParam.setId(id.intValue());
            updateParam.setBusinessId(businessId);
            updateParam.setBusinessType(businessType);
            updateParam.setIsCover(0);
            attachUpdateParams.add(updateParam);
        });
        attachApi.batchUpdateAttachByIdList(attachUpdateParams);
    }

    private List<AttachInfoBO> getAttachList(Long businessId) {
        AttachQuery attachQuery = new AttachQuery();
        attachQuery.setBusinessId(businessId.intValue());
        attachQuery.setBusinessType(businessType);
        List<AttachDTO> attachList = attachApi.getAttachInfoByParam(attachQuery);
        List<AttachInfoBO> result = new ArrayList<>();
        if (!CollectionUtils.isEmpty(attachList)) {
            attachList.forEach(attach -> {
                AttachInfoBO attachInfoBO = new AttachInfoBO();
                attachInfoBO.setUrl(url + attach.getFileUuid());
                result.add(attachInfoBO);
            });
        }
        return result;
    }

    public String create(OrderParam orderCreateParam) throws BusinessException {
        //1 去需求单抢个单 todo 暂时用乐观锁实现
        Boolean result = requestOrderApi.grabOrder(orderCreateParam.getRequestOrderNo());
        Asserts.isTrue(!result,new BusinessException(ResultEnum.ORDER_GRAB_FAIL));
        //2 创建订单 //todo 师傅有进行中的订单不允许再抢
        LoginUser user = SecurityContext.getLoginToken();
        String orderNo = uidGenerator.generate(user.getUserId());
        try {
            Long userId = getUserIdByToken();
            orderCreateParam.setUserId(userId);
            OrderService orderService = (OrderService) AopContext.currentProxy();
            orderService.doCreate(orderNo,orderCreateParam);
        } catch (BusinessException e) {
            // 出现任何异常,发送mq给需求单取消
            eventService.sendOrderCancelEvent(orderAssembler.assembleOrderMqDTO(orderCreateParam));
            log.error("生单时发生异常", e);
            throw e;
        }
        return orderNo;
    }

    private Long getUserIdByToken() throws BusinessException {
        LoginUser loginUser = SecurityContext.getLoginToken();
        Asserts.isTrue(loginUser == null,new BusinessException(ResultEnum.USER_TOKEN_VERIFY_FAILED));
        return loginUser.getUserId();
    }


    @Transactional(rollbackFor = Exception.class)
    public void doCreate(String orderNo, OrderParam orderCreateParam) throws BusinessException{
        orderCreateParam.setOrderNo(orderNo);
        orderCreateParam.setStatus(OrderStatusEnum.ORDER_CREATE_SUCCESS.getStatus());
        OrderLogParam logParam = orderAssembler.generateOrderLog(orderNo, orderCreateParam.getUserId(), OrderStatusEnum.ORDER_CREATE_SUCCESS);
        orderRepository.save(orderCreateParam);
        orderLogRepository.save(logParam);
    }


    @Transactional(rollbackFor = Exception.class)
    public OrderBO doUpdate(String orderNo, Long userId, OrderStatusEnum orderStatusEnum) throws BusinessException {
        OrderBO orderBO = orderRepository.getOrderByOrderNo(orderNo);
        Asserts.isTrue(orderBO == null,new BusinessException(ResultEnum.ORDER_EMPTY));
        Asserts.isTrue(OrderStatusEnum.isErrorStatus(orderBO.getStatus(), orderStatusEnum.getStatus()),
                new BusinessException(ResultEnum.ORDER_STATUS_TRANSFORM_ERROR)); ;
        Long id = orderBO.getId();
        OrderParam orderCreateParam = new OrderParam();
        orderCreateParam.setOrderNo(orderNo);
        orderCreateParam.setId(id);
        orderCreateParam.setStatus(orderStatusEnum.getStatus());
        OrderLogParam logParam = orderAssembler.generateOrderLog(orderNo, userId, orderStatusEnum);
        orderRepository.save(orderCreateParam);
        orderLogRepository.save(logParam);
        return orderBO;
    }



    public ListRecordTotalBO<OrderBO> list(OrderQuery orderQuery) throws BusinessException {
        Long userId = getUserIdByToken();
        orderQuery.setUserId(userId);
        Long totalRecord = this.orderRepository.getOrderCount(orderQuery);
        List<OrderBO> orderBoList = new ArrayList<>();
        if (totalRecord > 0) {
            orderBoList = this.orderRepository.queryOrders(orderQuery);
        }
        return new ListRecordTotalBO<>(orderBoList, totalRecord);
    }

    public OrderBO getOrderByOrderNo(String orderNo) throws BusinessException {
        OrderBO order = orderRepository.getOrderByOrderNo(orderNo);
        Asserts.isTrue(order == null,new BusinessException(ResultEnum.ORDER_EMPTY));
        try {
            List<AttachInfoBO> attachList = getAttachList(order.getId());
            order.setAttachInfoBO(attachList);
        }catch (Exception e){
            log.error("获取附件失败",e);
        }
        Map<String, Long> operateTime = getOperateTime(orderNo);
        order.setSignTime(operateTime.get(OrderStatusEnum.ORDER_SIGN_SUCCESS.getMessage()));
        order.setFinishTime(operateTime.get(OrderStatusEnum.ORDER_SERVICE_SUCCESS_UN_SETTLE.getMessage()));
        return order;
    }

    private Map<String,Long> getOperateTime(String orderNo){
        List<OrderLogBO> orderLogs = orderLogRepository.getOrderLogByOrderNo(orderNo);
        if (!CollectionUtils.isEmpty(orderLogs)){
            return orderLogs.stream()
                    .collect(Collectors.toMap(OrderLogBO::getOperateName, OrderLogBO::getOperateTime,(v1,v2)->v2));
        }
        return Collections.emptyMap();
    }

}
