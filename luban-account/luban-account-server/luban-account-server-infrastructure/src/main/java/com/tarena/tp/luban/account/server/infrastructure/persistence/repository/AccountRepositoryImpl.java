/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.server.infrastructure.persistence.repository;

import com.tarena.tp.luban.account.bo.AccountBO;
import com.tarena.tp.luban.account.po.Account;
import com.tarena.tp.luban.account.server.dao.AccountDAO;
import com.tarena.tp.luban.account.server.infrastructure.persistence.data.converter.AccountConverter;

import com.tarena.tp.luban.account.server.param.AccountParam;
import com.tarena.tp.luban.account.server.param.PaymentParam;
import com.tarena.tp.luban.account.server.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountRepositoryImpl implements AccountRepository {

    @Autowired
    private AccountDAO accountDao;

    @Autowired
    private AccountConverter accountConverter;


    @Override public Long create(AccountParam accountParam) {
        Account account = this.accountConverter.param2po(accountParam);
        this.accountDao.insert(account);
        return  account.getId();
    }

    @Override
    public AccountBO getAccountByUserId(Long userId){
        Account account = accountDao.getAccountByUserId(userId);
        return accountConverter.po2bo(account);
    }

    @Override
    public Long updateAmount(PaymentParam param) {
        Account account = accountConverter.param2po(param);
        return accountDao.updateAmount(account);
    }

}
