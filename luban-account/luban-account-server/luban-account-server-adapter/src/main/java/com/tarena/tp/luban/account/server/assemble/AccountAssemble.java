/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.server.assemble;

import com.tarena.tp.luban.account.bo.AccountBO;
import com.tarena.tp.luban.account.server.dto.AccountDTO;
import com.tarena.tp.luban.account.server.web.vo.BankVO;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
public class AccountAssemble {
    public AccountDTO assembleAccountDTO(AccountBO accountBO) {
        AccountDTO dto = new AccountDTO();
        if (accountBO == null) {
            return dto;
        }
        BeanUtils.copyProperties(accountBO, dto);
        return dto;
    }

}
