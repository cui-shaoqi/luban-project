/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.admin.manager;

import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tarena.tp.luban.account.admin.bo.AccountBO;
import com.tarena.tp.luban.account.admin.repository.AccountRepository;
import com.tarena.tp.luban.account.admin.protocol.param.AccountParam;
import com.tarena.tp.luban.account.admin.protocol.query.AccountQuery;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class AccountService {
    @Autowired
    private AccountRepository accountRepository;

    private void validateSaveAccount(
        AccountParam accountParam) throws BusinessException {
        //Asserts.isTrue(StringUtility.isNullOrEmpty(accountParam.getName()), SecurityAdminError.NAME_IS_EMPTY, AccountSuffix.name);
    }

    public Long saveAccount(
        AccountParam accountParam) throws BusinessException {
        this.validateSaveAccount(accountParam);
        return this.accountRepository.save(accountParam);
    }

    public Integer deleteAccount(Long accountId) throws BusinessException {
        //Asserts.isTrue(StringUtility.isNullOrEmpty(accountId), SecurityAdminError.Account_ID_IS_EMPTY);
        return this.accountRepository.delete(accountId);
    }

    public Integer enableAccount(String id) throws BusinessException {
        return this.accountRepository.enable(id);
    }

    public Integer disableAccount(String id) throws BusinessException {
        return this.accountRepository.disable(id);
    }

    public ListRecordTotalBO<AccountBO> queryAllAccount() {
        return queryAccount(null);
    }

    public ListRecordTotalBO<AccountBO> queryAccount(
        AccountQuery accountQuery) {
        Long totalRecord = this.accountRepository.getAccountCount(accountQuery);
        List<AccountBO> accountBoList = null;
        if (totalRecord > 0) {
            accountBoList = this.accountRepository.queryAccounts(accountQuery);
        }
        return new ListRecordTotalBO<>(accountBoList, totalRecord);
    }

    public AccountBO getAccount(
        Long accountId) throws BusinessException {
        //Asserts.isTrue(accountId == null, AccountAdminError.IS_EMPTY);
        return this.accountRepository.getAccount(accountId);
    }
}