/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.admin.infrastructure.persistence.repository;

import com.tarena.tp.luban.account.admin.dao.BankCardDAO;
import com.tedu.inn.protocol.enums.StatusRecord;
import com.tedu.inn.protocol.dao.StatusCriteria;
import com.tarena.tp.luban.account.admin.infrastructure.persistence.data.converter.BankCardConverter;
import com.tarena.tp.luban.account.po.BankCard;
import com.tarena.tp.luban.account.admin.bo.BankCardBO;
import com.tarena.tp.luban.account.admin.protocol.param.BankCardParam;
import com.tarena.tp.luban.account.admin.repository.BankCardRepository;
import com.tarena.tp.luban.account.admin.protocol.query.BankCardQuery;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BankCardRepositoryImpl implements BankCardRepository {
    @Autowired
    private BankCardConverter bankCardConverter;

    @Autowired
    private BankCardDAO bankCardDao;

    @Override public Long save(BankCardParam bankCardParam) {
        BankCard bankCard = this.bankCardConverter.param2po(bankCardParam);
        if (bankCard.getId() != null) {
            this.bankCardDao.update(bankCard);
            return bankCard.getId();
        }
        this.bankCardDao.insert(bankCard);
        return  bankCard.getId();
    }

    @Override public Integer delete(Long bankCardId) {
        return this.bankCardDao.delete(bankCardId);
    }

    @Override public Integer disable(String bankCardIds) {
        StatusCriteria statusCriteria = new StatusCriteria(bankCardIds, StatusRecord.DISABLE.getStatus());
        this.bankCardConverter.convertStatus(statusCriteria);
        return this.bankCardDao.changeStatus(statusCriteria);
    }

    @Override public Integer enable(String bankCardIds) {
        StatusCriteria statusCriteria = new StatusCriteria(bankCardIds, StatusRecord.ENABLE.getStatus());
        this.bankCardConverter.convertStatus(statusCriteria);
        return this.bankCardDao.changeStatus(statusCriteria);
    }

    @Override public BankCardBO getBankCard(Long bankCardId) {
        BankCard bankCard = this.bankCardDao.getEntity(bankCardId);
        return this.bankCardConverter.po2bo(bankCard);
    }

    @Override public List<BankCardBO> queryBankCards(
        BankCardQuery bankCardQuery) {
        List<BankCard> bankCardList = this.bankCardDao.queryBankCards(this.bankCardConverter.toDbPagerQuery(bankCardQuery));
        return this.bankCardConverter.poList2BoList(bankCardList);
    }

    @Override public Long getBankCardCount(BankCardQuery bankCardQuery) {
        return this.bankCardDao.countBankCard(this.bankCardConverter.toDbPagerQuery(bankCardQuery));
    }
}