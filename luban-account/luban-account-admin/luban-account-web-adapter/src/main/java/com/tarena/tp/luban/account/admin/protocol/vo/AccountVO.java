/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.admin.protocol.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccountVO {

    @ApiModelProperty("id")
    Long id;

    @ApiModelProperty("用户id")
    Long userId;

    @ApiModelProperty("用户姓名")
    String userName;

    @ApiModelProperty("用户电话")
    String userPhone;

    @ApiModelProperty("结算中金额")
    Long settlingAmount;

    @ApiModelProperty("总金额")
    Long totalAmount;

    @ApiModelProperty("状态")
    Integer status;

    @ApiModelProperty("创建时间")
    Long gmtCreate;
}