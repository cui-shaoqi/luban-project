/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.admin.controller;

import com.tarena.tp.luban.account.admin.assemble.BankCardAssemble;
import com.tarena.tp.luban.account.admin.bo.BankCardBO;
import com.tarena.tp.luban.account.admin.manager.BankCardService;
import com.tarena.tp.luban.account.admin.protocol.param.BankCardParam;
import com.tarena.tp.luban.account.admin.protocol.query.BankCardQuery;
import com.tarena.tp.luban.account.admin.protocol.vo.BankCardVO;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tedu.inn.protocol.pager.PagerResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("bankCard")
public class BankCardController {
    @Autowired
    private BankCardService bankCardService;

    @Autowired
    private BankCardAssemble bankCardAssemble;

    @PostMapping("search")
    @ResponseBody
    public PagerResult<BankCardVO> queryBankCards(BankCardQuery bankCardQuery) {
        ListRecordTotalBO<BankCardBO> bankCardListTotalRecord = this.bankCardService.queryBankCard(bankCardQuery);
        return this.bankCardAssemble.assemblePagerResult(bankCardListTotalRecord, bankCardQuery);
    }

    @PostMapping("save")
    @ResponseBody
    public Long saveBankCard(BankCardParam bankCardParam) throws BusinessException {
        return this.bankCardService.saveBankCard(bankCardParam);
    }

    @GetMapping("get")
    public BankCardVO getBankCard(Long bankCardId) throws BusinessException {
        BankCardBO bankCardBo = bankCardService.getBankCard(bankCardId);
        return this.bankCardAssemble.assembleBO2VO(bankCardBo);
    }

    @PostMapping("delete")
    public Integer deleteBankCard(Long id) throws BusinessException {
        return this.bankCardService.deleteBankCard(id);
    }

    @PostMapping("enable")
    public Integer enableBankCard(String ids) throws BusinessException {
        return this.bankCardService.enableBankCard(ids);
    }

    @PostMapping("disable")
    public Integer disableBankCard(String ids) throws BusinessException {
        return this.bankCardService.disableBankCard(ids);
    }
}