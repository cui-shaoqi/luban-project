/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.admin.assemble;

import com.tarena.tp.luban.account.admin.bo.BankCardBO;
import com.tarena.tp.luban.account.admin.protocol.vo.BankCardVO;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.pager.PagerResult;
import com.tedu.inn.protocol.pager.SimplePagerQuery;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class BankCardAssemble {
    public BankCardVO assembleBO2VO(BankCardBO bo) {
        BankCardVO bankCard = new BankCardVO();
        BeanUtils.copyProperties(bo, bankCard);
        return bankCard;
    }

    public List<BankCardVO> boListAssembleVOList(List<BankCardBO> list) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.emptyList();
        }
        List<BankCardVO> bankCardVOList = new ArrayList<>(list.size());
        for (BankCardBO bankCardBo : list) {
            BankCardVO bankCardVo = this.assembleBO2VO(bankCardBo);
            bankCardVOList.add(bankCardVo);
        }
        return bankCardVOList;
    }

    public PagerResult<BankCardVO> assemblePagerResult(ListRecordTotalBO<BankCardBO> bankCardListTotalRecord,
        SimplePagerQuery bankCardQuery) {
        List<BankCardVO> bankCardVOList = this.boListAssembleVOList(bankCardListTotalRecord.getList());
        PagerResult<BankCardVO> pagerResult = new PagerResult<>(bankCardQuery);
        pagerResult.setObjects(bankCardVOList);
        pagerResult.setTotal(bankCardListTotalRecord.getTotal());
        return pagerResult;
    }
}