/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.settle.admin.infrastructure.persistence.data.converter;

import com.tedu.inn.protocol.dao.StatusCriteria;
import com.tarena.tp.luban.settle.admin.bo.SettleBillBO;
import com.tarena.tp.luban.settle.po.SettleBill;
import com.tarena.tp.luban.settle.admin.protocol.param.SettleBillParam;
import com.tarena.tp.luban.settle.admin.protocol.query.SettleBillQuery;
import com.tarena.tp.luban.settle.admin.dao.query.SettleBillDBPagerQuery;
import org.springframework.beans.BeanUtils;
import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.sdk.context.SecurityContext;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class SettleBillConverter{

   public SettleBillDBPagerQuery toDbPagerQuery(SettleBillQuery settleBillQuery) {
           if (settleBillQuery == null) {
               return new SettleBillDBPagerQuery();
           }
           SettleBillDBPagerQuery settleBillDBPagerQuery = new SettleBillDBPagerQuery();
           BeanUtils.copyProperties(settleBillQuery, settleBillDBPagerQuery);
           return settleBillDBPagerQuery;
       }
   

     public SettleBill param2po(SettleBillParam param) {
        SettleBill settleBill = new SettleBill();
        BeanUtils.copyProperties(param, settleBill);

        LoginUser loginUser=SecurityContext.getLoginToken();

        settleBill.setGmtCreate(System.currentTimeMillis());
        settleBill.setGmtModified(settleBill.getGmtCreate());
        settleBill.setCreateUserId(loginUser.getUserId());
        settleBill.setModifiedUserId(loginUser.getUserId());
        settleBill.setStatus(1);

        settleBill.setCreateUserName(loginUser.getUserName());
        settleBill.setModifiedUserName(loginUser.getUserName());
        return settleBill;
    }

     public SettleBillBO po2bo(SettleBill settleBill) {
        SettleBillBO settleBillBO = new SettleBillBO();
        BeanUtils.copyProperties(settleBill, settleBillBO);
        return settleBillBO;
    }

     public List<SettleBillBO> poList2BoList(List<SettleBill> list) {
        List<SettleBillBO> settleBillBos = new ArrayList<>(list.size());
        for (SettleBill settleBill : list) {
            settleBillBos.add(this.po2bo(settleBill));
        }
        return settleBillBos;
    }

    public void convertStatus(StatusCriteria statusCriteria){
            LoginUser loginUser = SecurityContext.getLoginToken();
            statusCriteria.setModifiedUserId(loginUser.getUserId());
            statusCriteria.setModifiedUserName(loginUser.getUserName());
            statusCriteria.setGmtModified(System.currentTimeMillis());
    }
}