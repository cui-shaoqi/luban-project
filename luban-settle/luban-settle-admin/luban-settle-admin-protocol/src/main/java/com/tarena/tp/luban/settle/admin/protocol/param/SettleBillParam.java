/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.settle.admin.protocol.param;

public class SettleBillParam{
    private Long id; 
private String userId; 
private String type; 
private String requestOrderNo; 
private String orderNo; 
private String totalAmount; 
private String scale; 
private String amount; 
private Integer status; 
private String createUserName; 
private Long createUserId; 
private Long modifiedUserId; 
private String modifiedUserName; 
private Long gmtModified; 
private Long gmtCreate; 
public Long getId(){
 return this.id;
}
public void setId(Long id){
this.id=id;
}
public String getUserId(){
 return this.userId;
}
public void setUserId(String userId){
this.userId=userId;
}
public String getType(){
 return this.type;
}
public void setType(String type){
this.type=type;
}
public String getRequestOrderNo(){
 return this.requestOrderNo;
}
public void setRequestOrderNo(String requestOrderNo){
this.requestOrderNo=requestOrderNo;
}
public String getOrderNo(){
 return this.orderNo;
}
public void setOrderNo(String orderNo){
this.orderNo=orderNo;
}
public String getTotalAmount(){
 return this.totalAmount;
}
public void setTotalAmount(String totalAmount){
this.totalAmount=totalAmount;
}
public String getScale(){
 return this.scale;
}
public void setScale(String scale){
this.scale=scale;
}
public String getAmount(){
 return this.amount;
}
public void setAmount(String amount){
this.amount=amount;
}
public Integer getStatus(){
 return this.status;
}
public void setStatus(Integer status){
this.status=status;
}
public String getCreateUserName(){
 return this.createUserName;
}
public void setCreateUserName(String createUserName){
this.createUserName=createUserName;
}
public Long getCreateUserId(){
 return this.createUserId;
}
public void setCreateUserId(Long createUserId){
this.createUserId=createUserId;
}
public Long getModifiedUserId(){
 return this.modifiedUserId;
}
public void setModifiedUserId(Long modifiedUserId){
this.modifiedUserId=modifiedUserId;
}
public String getModifiedUserName(){
 return this.modifiedUserName;
}
public void setModifiedUserName(String modifiedUserName){
this.modifiedUserName=modifiedUserName;
}
public Long getGmtModified(){
 return this.gmtModified;
}
public void setGmtModified(Long gmtModified){
this.gmtModified=gmtModified;
}
public Long getGmtCreate(){
 return this.gmtCreate;
}
public void setGmtCreate(Long gmtCreate){
this.gmtCreate=gmtCreate;
}

}