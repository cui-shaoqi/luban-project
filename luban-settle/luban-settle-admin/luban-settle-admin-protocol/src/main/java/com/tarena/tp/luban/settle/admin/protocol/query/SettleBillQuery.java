/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.settle.admin.protocol.query;

import com.tedu.inn.protocol.pager.SimplePagerQuery;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.Date;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SettleBillQuery extends SimplePagerQuery {

    @ApiModelProperty("订单编号")
    String orderNo;

    @ApiModelProperty("师傅姓名")
    String workerName;

    @ApiModelProperty("师傅电话")
    String workerPhone;

    @ApiModelProperty("师傅电话")
    String requestUserName;

    @ApiModelProperty("师傅电话")
    String requestUserPhone;

    @ApiModelProperty("下单开始日期")
    Long start;

    @ApiModelProperty("下单结束日期")
    Long end;

    Integer type;
}