/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.settle.admin.manager;

import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tarena.tp.luban.settle.admin.bo.SettleBillBO;
import com.tarena.tp.luban.settle.admin.repository.SettleBillRepository;
import com.tarena.tp.luban.settle.admin.protocol.param.SettleBillParam;
import com.tarena.tp.luban.settle.admin.protocol.query.SettleBillQuery;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class SettleBillService {
    @Autowired
    private SettleBillRepository settleBillRepository;

    private void validateSaveSettleBill(
        SettleBillParam settleBillParam) throws BusinessException {
        //Asserts.isTrue(StringUtility.isNullOrEmpty(settleBillParam.getName()), SecurityAdminError.NAME_IS_EMPTY, SettleBillSuffix.name);
    }

    public Long saveSettleBill(
        SettleBillParam settleBillParam) throws BusinessException {
        this.validateSaveSettleBill(settleBillParam);
        return this.settleBillRepository.save(settleBillParam);
    }

    public Integer deleteSettleBill(Long settleBillId) throws BusinessException {
        //Asserts.isTrue(StringUtility.isNullOrEmpty(settleBillId), SecurityAdminError.SettleBill_ID_IS_EMPTY);
        return this.settleBillRepository.delete(settleBillId);
    }

    public Integer enableSettleBill(String settleBillIds) throws BusinessException {
        //Asserts.isTrue(StringUtils.isEmpty(bankIds), SettleBillAdminError.ID_IS_EMPTY);
        return this.settleBillRepository.enable(settleBillIds);
    }

    public Integer disableSettleBill(String settleBillIds) throws BusinessException {
        //Asserts.isTrue(StringUtils.isEmpty(settleBillIds), SettleBillAdminError.ID_IS_EMPTY);
        return this.settleBillRepository.disable(settleBillIds);
    }

    public ListRecordTotalBO<SettleBillBO> queryAllSettleBill() {
        return querySettleBill(null);
    }

    public ListRecordTotalBO<SettleBillBO> querySettleBill(
        SettleBillQuery settleBillQuery) {
        Long totalRecord = this.settleBillRepository.getSettleBillCount(settleBillQuery);
        List<SettleBillBO> settleBillBoList = null;
        if (totalRecord > 0) {
            settleBillBoList = this.settleBillRepository.querySettleBills(settleBillQuery);
        }
        return new ListRecordTotalBO<>(settleBillBoList, totalRecord);
    }

    public SettleBillBO getSettleBill(
        Long settleBillId) throws BusinessException {
        //Asserts.isTrue(settleBillId == null, SettleBillAdminError.IS_EMPTY);
        return this.settleBillRepository.getSettleBill(settleBillId);
    }
}