/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.settle.server.infrastructure.persistence.repository;

import com.tarena.tp.luban.settle.po.SettleBill;
import com.tarena.tp.luban.settle.server.bo.SettleBillBO;
import com.tarena.tp.luban.settle.server.dao.SettleBillDAO;
import com.tarena.tp.luban.settle.server.infrastructure.persistence.converter.SettleBillConverter;
import com.tarena.tp.luban.settle.server.protocol.param.SettleBillParam;
import com.tarena.tp.luban.settle.server.repository.SettleBillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class SettleBillRepositoryImpl implements SettleBillRepository {

    @Autowired
    private SettleBillConverter settleBillConverter;

    @Autowired
    private SettleBillDAO settleBillDao;

    @Override
    public void batchSave(List<SettleBillParam> settleBillParams) {
        List<SettleBill> settleBills = this.settleBillConverter.params2pos(settleBillParams);
        this.settleBillDao.batchInsert(settleBills);
    }

    @Override
    public void updateStatus(String orderNo, Integer status) {
        SettleBill settleBill = settleBillConverter.param2UpdatePO(orderNo, status);
        settleBillDao.updateStatus(settleBill);
    }

    @Override
    public Long getSettleBillByOrderNo(String orderNo) {
        List<SettleBill> settleBills = settleBillDao.getSettleBillByOrderNo(orderNo);
        return (long) settleBills.size();
    }
}