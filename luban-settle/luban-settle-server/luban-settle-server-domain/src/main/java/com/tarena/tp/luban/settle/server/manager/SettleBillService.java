/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.settle.server.manager;

import com.tarena.tp.luban.account.server.client.AccountApi;
import com.tarena.tp.luban.account.server.param.PaymentParam;
import com.tarena.tp.luban.order.server.protocol.dto.OrderMqDTO;
import com.tarena.tp.luban.settle.common.enums.SettleStatusEnum;
import com.tarena.tp.luban.settle.common.enums.SettleTypeEnum;
import com.tarena.tp.luban.settle.server.protocol.param.SettleBillParam;
import com.tarena.tp.luban.settle.server.repository.SettleBillRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SettleBillService {


    @Autowired
    private SettleBillRepository settleBillRepository;

    @Autowired
    private EventService eventService;

    @Autowired
    private AccountApi accountApi;

    public void settle(OrderMqDTO settleDTO) {
        Long size = settleBillRepository.getSettleBillByOrderNo(settleDTO.getOrderNo());
        if (size == 0) {
            //通过需求单号查询供应商信息 包括基本信息和分润信息
            List<SettleBillParam> settleBillParams = buildSettleBillParam(settleDTO);
            //插入账单流水表
            this.settleBillRepository.batchSave(settleBillParams);
            //通知财务
            eventService.sendSettledEvent(settleDTO);
        }
    }


    private List<SettleBillParam> buildSettleBillParam(OrderMqDTO settleDTO){
        String requestOrderNo = settleDTO.getRequestOrderNo();
        List<SettleBillParam> paramList = new ArrayList<>();
        //展示价格
        Long requestOrderPrice = settleDTO.getRequestOrderPrice();
        //原始价格
        Long requestOrderRawPrice = settleDTO.getRequestOrderRawPrice();
        SettleBillParam worderSettleBillParam  = buildSettleBillParam(settleDTO,requestOrderPrice,
                SettleTypeEnum.SETTLE_TYPE_WORKER.getStatus(), settleDTO.getWorkerId());
        SettleBillParam platformSettleBillParam  = buildSettleBillParam(settleDTO,
                getPlatformAmount(requestOrderRawPrice,requestOrderPrice),
                SettleTypeEnum.SETTLE_TYPE_PLATFORM.getStatus(),999L);
        paramList.add(platformSettleBillParam);
        paramList.add(worderSettleBillParam);
        return paramList;
    }


    private Long getPlatformAmount(Long rawPrice,Long workerPrice){
        return new BigDecimal(rawPrice).subtract(new BigDecimal(workerPrice)).longValue();
    }

    private SettleBillParam buildSettleBillParam(OrderMqDTO settleDTO,Long amount,Integer type,Long userId){
        SettleBillParam settleBillParam = new SettleBillParam();
        BeanUtils.copyProperties(settleDTO,settleBillParam);
        settleBillParam.setAmount(amount);
        settleBillParam.setType(type);
        settleBillParam.setUserId(userId);
        settleBillParam.setScale(settleDTO.getProfitScale());
        settleBillParam.setRequestUserId(settleDTO.getUserId());
        settleBillParam.setRequestUserName(settleDTO.getUserName());
        settleBillParam.setRequestUserPhone(settleDTO.getUserPhone());
        settleBillParam.setStatus(SettleStatusEnum.SETTLE_STATUS_WAIT.getStatus());
        settleBillParam.setOrderGmtCreate(settleDTO.getGmtCreate());
        settleBillParam.setTotalAmount(amount);
        return settleBillParam;
    }

    public void mockPayAndUpdateSettleBillStatus(OrderMqDTO orderMqDTO) {
        PaymentParam param = new PaymentParam();
        param.setUserId(orderMqDTO.getWorkerId());
        param.setOrderNo(orderMqDTO.getOrderNo());
        param.setTotalAmount(orderMqDTO.getRequestOrderPrice());
        log.info("pay req:{}",param);
        Long result = accountApi.mockPayment(param);
        if (result > 0) {
            log.info("pay success req:{},resp:{}",param,result);
            settleBillRepository.updateStatus(orderMqDTO.getOrderNo(),SettleStatusEnum.SETTLE_STATUS_STEELED.getStatus());
        }else {
            log.info("pay failed req:{},resp:{}",param,result);
        }
    }

    @Data
    @FieldDefaults(level = AccessLevel.PRIVATE)
    static class ProviderDTO {
        Long id;
        String name;
        BigDecimal profitScale;
    }
}
