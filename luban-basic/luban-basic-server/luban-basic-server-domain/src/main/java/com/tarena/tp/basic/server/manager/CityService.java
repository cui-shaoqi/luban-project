/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.basic.server.manager;

import com.tarena.tp.basic.server.persists.repository.CityRepository;
import com.tarena.tp.basic.server.dto.CityDTO;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RList;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
@Slf4j
public class CityService {

    @Resource
    private CityRepository cityRepository;

    public Map<Long, List<CityDTO>> queryList() {
        return cityRepository.queryList();
    }


    @Resource
    private RedissonClient client;


    public void initCityData(){
        List<CityDTO> cityDTOS = queryCityTreeFromDB();
        RList<CityDTO> city = client.getList("city");
        if (city.isEmpty()) {
            //todo 需考虑多节点
            city.addAll(cityDTOS);
        }
    }


    public List<CityDTO> queryCityTree(){
        return client.getList("city");
    }

    public List<CityDTO> queryCityTreeFromDB() {
        List<CityDTO> result = new ArrayList<>();
        //获取全部区域数据
        Map<Long,List<CityDTO>> allAreaMap = cityRepository.queryList();
        //获取一级分类
        List<CityDTO> firstAreaList = allAreaMap.get(0L);
        for (CityDTO cityDTO : firstAreaList) {
            result.add(cityDTO);
            //获取二级分类
            List<CityDTO> secondAreaList = allAreaMap.get(cityDTO.getId());
            if (CollectionUtils.isEmpty(secondAreaList)) {
                continue;
            }
            cityDTO.setChildren(secondAreaList);
            //获取三级分类
            for (CityDTO secondArea : cityDTO.getChildren()) {
                List<CityDTO> thirdAreaList = allAreaMap.get(secondArea.getId());
                secondArea.setChildren(thirdAreaList);
            }
        }
        return result;
    }

    public String getAreaName(Long areaId) {
        String areaName = "";
        CityDTO area = cityRepository.queryAreaById(areaId);
        areaName = area.getName();
        if (area.getParentId() != 0) {
            CityDTO city = cityRepository.queryAreaById(area.getParentId());
            areaName = city.getName() + areaName;
            if (city.getParentId() != 0) {
                CityDTO province = cityRepository.queryAreaById(city.getParentId());
                areaName = province.getName() + areaName;
            }
        }
        return areaName;
    }
}
