package com.tarena.tp.basic.server.manager;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Slf4j
@Service
public class CityLoader implements ApplicationRunner {

    @Resource
    private CityService cityService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        long start = System.currentTimeMillis();
        log.info("start init city data start{}",start);
        cityService.initCityData();
        long end = System.currentTimeMillis();
        log.info("success init city data start:{}, end: {},cost:{}",start,end,end-start);
    }
}
