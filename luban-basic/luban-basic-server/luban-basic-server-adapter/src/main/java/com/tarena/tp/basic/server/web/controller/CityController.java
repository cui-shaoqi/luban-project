/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.basic.server.web.controller;

import com.tarena.tp.basic.server.manager.CityService;
import com.tarena.tp.basic.server.dto.CityDTO;
import com.tarena.tp.basic.server.web.assemble.CityAssemble;
import com.tarena.tp.basic.server.web.vo.CityVO;
import com.tedu.inn.protocol.model.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "city", tags = "城市管理")
@Slf4j
@Validated
@RestController
@RequestMapping("city")
public class CityController {

    @Autowired
    private CityService cityService;

    @Autowired
    private CityAssemble cityAssemble;

    @GetMapping("tree")
    @ApiOperation(value = "返回三级城市树")
    public Result<List<CityVO>> buildCityTree() {
        List<CityDTO> cityDtoList = cityService.queryCityTree();
        return new Result<>(cityAssemble.assembleDtosToVos(cityDtoList));
    }

    @GetMapping("area/name")
    @ApiOperation(value = "获取地址全名称")
    public Result<String> getAreName(Long areaId) {
        return new Result<>(cityService.getAreaName(areaId));
    }

}
