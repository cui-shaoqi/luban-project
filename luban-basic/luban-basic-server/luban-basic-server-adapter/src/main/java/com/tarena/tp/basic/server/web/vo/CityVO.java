/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.basic.server.web.vo;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
public class CityVO {

    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("父级ID")
    private Long parentId;

    @ApiModelProperty("地址编码")
    private String code;

    @ApiModelProperty("城市编码")
    private String cityCode;

    @ApiModelProperty("省/市/区名称")
    private String name;

    @ApiModelProperty("1省2市3区")
    private Integer depth;

    @ApiModelProperty("是否删除0未删除1已删除")
    private Integer isDelete;

    @ApiModelProperty("是否是直辖市1是0否")
    private Integer isMunicipality;

    @ApiModelProperty("创建人ID")
    private Long createUserId;

    @ApiModelProperty("创建时间")
    private Long gmtCreate;

    @ApiModelProperty("修改人ID")
    private Long modifiedUserId;

    @ApiModelProperty("修改时间")
    private Long gmtModified;

    @ApiModelProperty("创建人")
    private String createUserName;

    @ApiModelProperty("修改人")
    private String modifiedUserName;

    @ApiModelProperty("子结点")
    private List<CityVO> children;
}
