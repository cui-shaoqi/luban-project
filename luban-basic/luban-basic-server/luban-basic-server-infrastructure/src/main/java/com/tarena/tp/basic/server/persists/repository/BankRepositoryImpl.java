/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.basic.server.persists.repository;

import com.tarena.tp.basic.po.Bank;
import com.tarena.tp.basic.server.dao.BankMapper;
import com.tarena.tp.basic.server.dto.BankDTO;
import com.tarena.tp.basic.server.persists.data.converter.BankConverter;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BankRepositoryImpl implements BankRepository {
    @Autowired
    private BankMapper bankMapper;

    @Autowired
    private BankConverter bankConverter;

    @Override public List<BankDTO> queryEnabledBankList() {
        List<Bank> banks = this.bankMapper.queryEnabledBankList();
        return this.bankConverter.convertDtoList(banks);
    }
}
