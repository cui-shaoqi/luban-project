/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.basic.admin.persists.data.converter;

import com.tarena.tp.basic.admin.dto.CityDTO;
import com.tarena.tp.basic.po.City;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CityConverter {

    public List<CityDTO> convertModelListToDtoList(List<City> sources) {
        if (sources == null) {
            return null;
        }
        List<CityDTO> objects = new ArrayList<>();
        for (City source : sources) {
            CityDTO object = new CityDTO();
            BeanUtils.copyProperties(source,object);
            objects.add(object);
        }
        return objects;
    }

    public CityDTO convertModelToDto(City source) {
        if (source == null) {
            return null;
        }
        CityDTO object = new CityDTO();
        BeanUtils.copyProperties(source, object);
        return object;
    }
}
