/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.basic.admin.persists.repository;

import com.tarena.tp.basic.admin.dao.CodeMapper;
import com.tarena.tp.basic.admin.dto.CityDTO;
import com.tarena.tp.basic.admin.persists.data.converter.CodeConverter;
import com.tarena.tp.basic.admin.dto.CodeDTO;
import com.tarena.tp.basic.admin.query.CodeQuery;
import com.tarena.tp.basic.admin.repository.CodeRepository;
import com.tarena.tp.basic.po.City;
import com.tarena.tp.basic.po.Code;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CodeRepositoryImpl implements CodeRepository {

    @Resource
    private CodeConverter codeConverter;

    @Resource
    private CodeMapper codeMapper;

    @Override
    public CodeDTO getCodeInfoByParam(CodeQuery codeQuery) {
        Code code = codeMapper.getCodeInfoByParam(codeQuery);
        return codeConverter.convertModelToDto(code);
    }

    @Override
    public Map<Integer, List<CodeDTO>> queryList(CodeQuery codeQuery) {
        List<Code> areaList = codeMapper.queryList(codeQuery);
        List<CodeDTO> areaVoList = codeConverter.convertModelListToDtoList(areaList);
        return areaVoList.stream().collect(Collectors.groupingBy(CodeDTO::getParentId));
    }

}
