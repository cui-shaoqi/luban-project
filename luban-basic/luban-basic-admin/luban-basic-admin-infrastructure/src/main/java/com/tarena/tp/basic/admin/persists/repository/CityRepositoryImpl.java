/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.basic.admin.persists.repository;

import com.tarena.tp.basic.admin.dao.CityMapper;
import com.tarena.tp.basic.admin.persists.data.converter.CityConverter;
import com.tarena.tp.basic.admin.dto.CityDTO;
import com.tarena.tp.basic.admin.repository.CityRepository;
import com.tarena.tp.basic.po.City;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CityRepositoryImpl implements CityRepository {

    @Resource
    private CityConverter cityConverter;

    @Resource
    private CityMapper cityMapper;

    @Override
    public Map<Long, List<CityDTO>> queryList() {
        List<City> areaList = cityMapper.queryList();
        List<CityDTO> areaVoList = cityConverter.convertModelListToDtoList(areaList);
        return areaVoList.stream().collect(Collectors.groupingBy(CityDTO::getParentId));
    }

    @Override
    public CityDTO queryAreaById(Long id) {
        return cityConverter.convertModelToDto(cityMapper.selectByPrimaryKey(id));
    }

}
