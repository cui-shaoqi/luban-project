/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.basic.admin.web.controller;

import com.tarena.tp.basic.admin.dto.CityDTO;
import com.tarena.tp.basic.admin.manager.CodeService;
import com.tarena.tp.basic.admin.dto.CodeDTO;
import com.tarena.tp.basic.admin.query.CodeQuery;
import com.tarena.tp.basic.admin.web.assemble.CodeAssemble;
import com.tarena.tp.basic.admin.web.vo.CityVO;
import com.tarena.tp.basic.admin.web.vo.CodeVO;
import com.tedu.inn.protocol.model.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "code", tags = "编码管理")
@Slf4j
@Validated
@RestController
@RequestMapping("code")
public class CodeController {

    @Autowired
    private CodeService codeService;

    @Autowired
    private CodeAssemble codeDtoVoAssemble;

    @GetMapping("query")
    @ApiOperation(value = "获取编码值")
    public Result<CodeVO> getCodeInfoByParam(CodeQuery codeQuery) {
        CodeDTO codeDto = codeService.getCodeInfoByParam(codeQuery);
        return new Result<>(codeDtoVoAssemble.convertDTOToVO(codeDto));
    }

    @GetMapping("tree")
    @ApiOperation(value = "获取编码和子编码")
    public Result<List<CodeVO>> buildCityTree(CodeQuery codeQuery) {
        List<CodeDTO> codeDtoList = codeService.queryCode(codeQuery);
        return new Result<>(codeDtoVoAssemble.convertDtosToVos(codeDtoList));
    }

}
