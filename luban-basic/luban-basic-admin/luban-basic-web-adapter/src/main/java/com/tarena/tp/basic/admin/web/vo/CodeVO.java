/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.basic.admin.web.vo;

import com.tarena.tp.basic.admin.dto.CodeDTO;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;

import java.util.List;

@Data
public class CodeVO {

    @ApiModelProperty("id")
    private Integer id;

    @ApiModelProperty("父级ID")
    private Integer parentId;

    @ApiModelProperty("appid")
    private Integer appId;

    @ApiModelProperty("名字")
    private String name;

    @ApiModelProperty("编码key")
    private String code;

    @ApiModelProperty("编码值")
    private String value;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("状态")
    private Integer status;

    @ApiModelProperty("子节点")
    private List<CodeVO> children;

}
