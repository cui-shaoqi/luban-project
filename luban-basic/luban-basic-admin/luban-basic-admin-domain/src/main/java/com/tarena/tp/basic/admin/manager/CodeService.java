/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.basic.admin.manager;

import com.tarena.tp.basic.admin.dto.CityDTO;
import com.tarena.tp.basic.admin.repository.CodeRepository;
import com.tarena.tp.basic.admin.dto.CodeDTO;
import com.tarena.tp.basic.admin.query.CodeQuery;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class CodeService {

    @Resource
    private CodeRepository codeRepository;

    public CodeDTO getCodeInfoByParam(CodeQuery codeQuery) {
        return codeRepository.getCodeInfoByParam(codeQuery);
    }




    public List<CodeDTO> queryCode(CodeQuery codeQuery) {
        List<CodeDTO> result = new ArrayList<>();
        //获取全部区域数据
        Map<Integer, List<CodeDTO>> allAreaMap = codeRepository.queryList(codeQuery);
        //获取一级分类
        List<CodeDTO> firstAreaList = allAreaMap.get(0);
        for (CodeDTO codeDTO : firstAreaList) {
            result.add(codeDTO);
            //获取二级分类
            List<CodeDTO> secondAreaList = allAreaMap.get(codeDTO.getId());
            if (CollectionUtils.isEmpty(secondAreaList)) {
                continue;
            }
            codeDTO.setChildren(secondAreaList);
            //获取三级分类
            for (CodeDTO secondArea : codeDTO.getChildren()) {
                List<CodeDTO> thirdAreaList = allAreaMap.get(secondArea.getId());
                secondArea.setChildren(thirdAreaList);
            }
        }
        return result;
    }

}
