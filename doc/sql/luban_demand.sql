/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.20 : Database - tarena_tp_luban_demand
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tarena_tp_luban_demand` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `tarena_tp_luban_demand`;

/*Table structure for table `access_provider` */

DROP TABLE IF EXISTS `access_provider`;

CREATE TABLE `access_provider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_name` varchar(32) NOT NULL DEFAULT '' COMMENT '供应商名称',
  `private_key` varchar(512) DEFAULT '' COMMENT '私钥',
  `public_key` varchar(512) DEFAULT '' COMMENT '公钥',
  `description` varchar(512) NOT NULL DEFAULT '' COMMENT '描述',
  `address` varchar(128) NOT NULL DEFAULT '' COMMENT '地址',
  `principal` varchar(32) NOT NULL DEFAULT '' COMMENT '负责人',
  `principal_tel` varchar(16) NOT NULL DEFAULT '' COMMENT '负责人电话',
  `profit_scale` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '分润比例',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `audit_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '审核状态',
  `create_user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `create_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `modified_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `modified_user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `gmt_modified` bigint(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `gmt_create` bigint(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='access_provider';

/*Data for the table `access_provider` */

insert  into `access_provider`(`id`,`provider_name`,`private_key`,`public_key`,`description`,`address`,`principal`,`principal_tel`,`profit_scale`,`status`,`audit_status`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_modified`,`gmt_create`) values (6,'北京达内科技有限公司',NULL,NULL,'IT培训班选达内IT培训机构,专注IT培训20年,开设Java大数据、Python人工智能、网络安全与运维、UI设计、运营、影视特效等IT培训、泛IT培训和非IT培训共12大IT课程,进官网了解课程详解及优惠','北京市海淀区中鼎大厦7层','侯亚强','13856781234',20.00,1,1,'mock',0,0,'mock',1683687427978,1683687415441);

/*Table structure for table `provider_audit_log` */

DROP TABLE IF EXISTS `provider_audit_log`;

CREATE TABLE `provider_audit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT '0' COMMENT '审核人id',
  `user_name` varchar(16) DEFAULT '' COMMENT '审核人姓名',
  `provider_id` int(10) unsigned DEFAULT '0' COMMENT '供应商id',
  `operate_time` bigint(20) DEFAULT '0' COMMENT '操作时间',
  `operate_name` varchar(16) DEFAULT '' COMMENT '操作步骤',
  `audit_status` tinyint(1) DEFAULT '0' COMMENT '状态 0:驳回，1:通过',
  `reject_reason` varchar(64) DEFAULT '' COMMENT '驳回原因',
  `remark` varchar(64) DEFAULT '' COMMENT '备注',
  `create_user_name` varchar(16) DEFAULT '' COMMENT '创建人',
  `create_user_id` int(10) unsigned DEFAULT '0' COMMENT '创建人id',
  `modified_user_id` int(10) unsigned DEFAULT '0' COMMENT '修改人id',
  `modified_user_name` varchar(16) DEFAULT '' COMMENT '修改人',
  `gmt_create` bigint(20) DEFAULT '0' COMMENT '创建时间',
  `gmt_modified` bigint(20) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COMMENT='供应商审核记录';

/*Data for the table `provider_audit_log` */

insert  into `provider_audit_log`(`id`,`user_id`,`user_name`,`provider_id`,`operate_time`,`operate_name`,`audit_status`,`reject_reason`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`) values (19,0,'mock',6,1683687427993,'审核',1,NULL,NULL,'mock',0,0,'mock',1683687427993,1683687427993);

/*Table structure for table `request_order` */

DROP TABLE IF EXISTS `request_order`;

CREATE TABLE `request_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_order_no` varchar(64) DEFAULT '' COMMENT '订单编号',
  `order_name` varchar(64) DEFAULT '' COMMENT '订单名称',
  `order_category_id` int(11) DEFAULT '0' COMMENT '订单类型',
  `order_category_name` varchar(64) DEFAULT '' COMMENT '订单类型中文名',
  `order_amount` int(11) DEFAULT '0' COMMENT '订单金额',
  `user_name` varchar(64) DEFAULT '' COMMENT '用户姓名',
  `user_phone` varchar(16) DEFAULT '' COMMENT '用户电话',
  `area_id` int(11) DEFAULT '0' COMMENT '服务地址id',
  `address` varchar(64) DEFAULT '' COMMENT '服务地址',
  `service_time` bigint(11) NOT NULL DEFAULT '0' COMMENT '服务时间',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态 0:无效，1:有效',
  `provider_id` int(11) DEFAULT '0' COMMENT '供应商id',
  `grab_status` tinyint(4) DEFAULT '0' COMMENT '抢单状态 0:未抢 1:已抢',
  `version` int(11) DEFAULT '0' COMMENT '被修改的次数',
  `create_user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `create_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `modified_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `modified_user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `gmt_modified` bigint(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `gmt_create` bigint(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='request_order';

/*Data for the table `request_order` */

insert  into `request_order`(`id`,`request_order_no`,`order_name`,`order_category_id`,`order_category_name`,`order_amount`,`user_name`,`user_phone`,`area_id`,`address`,`service_time`,`status`,`provider_id`,`grab_status`,`version`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_modified`,`gmt_create`) values (1,'32232964103521','床头柜安装199元',1,'衣柜安装',100,'张三','13266668888',1,'北京市海淀区中鼎大厦7层',1681897784819,1,6,1,1,'root',99,99,'root',1681897784819,1681897784819);
insert  into `request_order`(`id`,`request_order_no`,`order_name`,`order_category_id`,`order_category_name`,`order_amount`,`user_name`,`user_phone`,`area_id`,`address`,`service_time`,`status`,`provider_id`,`grab_status`,`version`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_modified`,`gmt_create`) values (2,'123123123','厨房抽油烟机安装',2,'厨具安装',100,'王五','13012345678',2,'北京市海淀区中鼎大厦7层',1681897784819,1,6,1,1,'root',99,99,'root',1681971710614,1681971710614);
insert  into `request_order`(`id`,`request_order_no`,`order_name`,`order_category_id`,`order_category_name`,`order_amount`,`user_name`,`user_phone`,`area_id`,`address`,`service_time`,`status`,`provider_id`,`grab_status`,`version`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_modified`,`gmt_create`) values (3,'32232964103522','衣柜安装399元',1,'衣柜安装',100,'张三','13266668888',1,'北京市海淀区中鼎大厦7层',1681897784819,1,6,1,1,'root',99,99,'root',1682057054354,1682057054354);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
