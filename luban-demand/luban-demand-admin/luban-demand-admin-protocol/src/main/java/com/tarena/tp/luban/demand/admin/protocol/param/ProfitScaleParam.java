package com.tarena.tp.luban.demand.admin.protocol.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProfitScaleParam {

    @ApiModelProperty("供应商id")
    private Long id;

    @ApiModelProperty("分润比例")
    private BigDecimal profitScale;

}
