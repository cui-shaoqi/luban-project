/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.demand.admin.assemble;

import com.tarena.tp.luban.demand.admin.bo.AccessProviderBO;
import com.tarena.tp.luban.demand.admin.protocol.vo.AccessProviderListItemVO;
import com.tarena.tp.luban.demand.admin.protocol.vo.AccessProviderVO;
import com.tarena.tp.luban.demand.admin.protocol.vo.ProviderAuditLogVO;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.pager.PagerResult;
import com.tedu.inn.protocol.pager.SimplePagerQuery;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class AccessProviderAssemble {

    public AccessProviderVO assembleBO2AccessProviderVO(AccessProviderBO bo) {
        AccessProviderVO accessProvider = new AccessProviderVO();
        BeanUtils.copyProperties(bo, accessProvider);
        return accessProvider;
    }

    public AccessProviderListItemVO assembleBO2VO(AccessProviderBO bo) {
        AccessProviderListItemVO accessProvider = new AccessProviderListItemVO();
        BeanUtils.copyProperties(bo, accessProvider);
        return accessProvider;
    }

    public List<AccessProviderListItemVO> boListAssembleVOList(List<AccessProviderBO> list) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.emptyList();
        }
        List<AccessProviderListItemVO> accessProviderVOList = new ArrayList<>(list.size());
        for (AccessProviderBO accessProviderBo : list) {
            AccessProviderListItemVO accessProviderVo = this.assembleBO2VO(accessProviderBo);
            accessProviderVOList.add(accessProviderVo);
        }
        return accessProviderVOList;
    }

    public PagerResult<AccessProviderListItemVO> assemblePagerResult(ListRecordTotalBO<AccessProviderBO> accessProviderListTotalRecord,
        SimplePagerQuery accessProviderQuery) {
        List<AccessProviderListItemVO> accessProviderVOList = this.boListAssembleVOList(accessProviderListTotalRecord.getList());
        PagerResult<AccessProviderListItemVO> pagerResult = new PagerResult<>(accessProviderQuery);
        pagerResult.setObjects(accessProviderVOList);
        pagerResult.setTotal(accessProviderListTotalRecord.getTotal());
        return pagerResult;
    }

}