/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.demand.admin.manager;

import com.tarena.tp.attach.server.client.AttachApi;
import com.tarena.tp.attach.server.dto.AttachDTO;
import com.tarena.tp.attach.server.param.AttachUpdateParam;
import com.tarena.tp.attach.server.query.AttachQuery;
import com.tarena.tp.luban.demand.admin.bo.ProviderAuditLogBO;
import com.tarena.tp.luban.demand.admin.protocol.param.ProfitScaleParam;
import com.tarena.tp.luban.demand.admin.protocol.param.ProviderAuditLogParam;
import com.tarena.tp.luban.demand.admin.repository.ProviderAuditLogRepository;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tarena.tp.luban.demand.admin.bo.AccessProviderBO;
import com.tarena.tp.luban.demand.admin.repository.AccessProviderRepository;
import com.tarena.tp.luban.demand.admin.protocol.param.AccessProviderParam;
import com.tarena.tp.luban.demand.admin.protocol.query.AccessProviderQuery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;


@Slf4j
@Service
public class AccessProviderService {
    @Autowired
    private AccessProviderRepository accessProviderRepository;

    @Autowired
    private ProviderAuditLogRepository providerAuditLogRepository;

    @Autowired
    private AttachApi attachApi;

    @Value("${app.business.type}")
    private Integer businessType;

    @Value("${url}")
    private String url;


    public Long saveAccessProvider(
        AccessProviderParam accessProviderParam) throws BusinessException {
        Long id = this.accessProviderRepository.save(accessProviderParam);
        try {
            createAttach(id.intValue(), Collections.singletonList(accessProviderParam.getAttachId()));
        }catch (Exception e) {
            log.error("文件绑定失败",e);
        }
        return id;
    }

    private void createAttach(Integer businessId,List<Long> fileIds) {
        List<AttachUpdateParam> attachUpdateParams = new ArrayList<>();
        // fileIds 文件id 前端入参
        fileIds.forEach(id -> {
            AttachUpdateParam updateParam = new AttachUpdateParam();
            updateParam.setId(id.intValue());
            updateParam.setBusinessId(businessId);
            updateParam.setBusinessType(businessType);
            updateParam.setIsCover(0);
            attachUpdateParams.add(updateParam);
        });
        attachApi.batchUpdateAttachByIdList(attachUpdateParams);
    }

    private List<String> getAttachList(Long businessId) {
        AttachQuery attachQuery = new AttachQuery();
        attachQuery.setBusinessId(businessId.intValue());
        attachQuery.setBusinessType(businessType);
        List<AttachDTO> attachList = attachApi.getAttachInfoByParam(attachQuery);
        List<String> result = new ArrayList<>();
        if (!CollectionUtils.isEmpty(attachList)) {
            attachList.forEach(attach -> {
                result.add(url + attach.getFileUuid());
            });
        }
        return result;
    }

    public AccessProviderBO getAccessProvider(
            Long accessProviderId) throws BusinessException {
        AccessProviderBO accessProvider = accessProviderRepository.getAccessProvider(accessProviderId);
        if (accessProvider != null) {
            try {
                List<String> attachList = getAttachList(accessProvider.getId());
                if (!CollectionUtils.isEmpty(attachList)) {
                    accessProvider.setAttachUrl(attachList.get(0));
                }
            }catch (Exception e) {
                log.error("获取文件失败",e);
            }
        }
        return accessProvider;
    }

    public AccessProviderBO processProviderVO(
            Long accessProviderId) throws BusinessException {

        AccessProviderBO accessProvider = this.getAccessProvider(accessProviderId);
        List<ProviderAuditLogBO> providerAuditLogBO = this.providerAuditLogRepository.getAuditLogByProviderId(accessProviderId);

        AccessProviderBO processProviderBO = new AccessProviderBO();
        BeanUtils.copyProperties(accessProvider,processProviderBO);

        processProviderBO.setProviderAuditLog(providerAuditLogBO);

        return processProviderBO;
    }

    public Integer processProvider(ProviderAuditLogParam providerAuditLogParam){
        AccessProviderParam accessProviderParam = new AccessProviderParam();
        accessProviderParam.setId(providerAuditLogParam.getProviderId());
        accessProviderParam.setAuditStatus(providerAuditLogParam.getAuditStatus());
        this.accessProviderRepository.save(accessProviderParam);
        return this.providerAuditLogRepository.saveProviderAuditLog(providerAuditLogParam);

    }



    public Integer updateProfitScale(ProfitScaleParam profitScaleParam) throws BusinessException {
        return this.accessProviderRepository.updateProfitScale(profitScaleParam);
    }

    public Integer enableAccessProvider(String accessProviderId) throws BusinessException {
        //Asserts.isTrue(StringUtils.isEmpty(bankIds), AccessProviderAdminError.ID_IS_EMPTY);
        return this.accessProviderRepository.enable(accessProviderId);
    }

    public Integer disableAccessProvider(String accessProviderId) throws BusinessException {
        //Asserts.isTrue(StringUtils.isEmpty(accessProviderIds), AccessProviderAdminError.ID_IS_EMPTY);
        return this.accessProviderRepository.disable(accessProviderId);
    }

    public ListRecordTotalBO<AccessProviderBO> queryAccessProvider(
        AccessProviderQuery accessProviderQuery) {
        Long totalRecord = this.accessProviderRepository.getAccessProviderCount(accessProviderQuery);
        List<AccessProviderBO> accessProviderBoList = new ArrayList<>();
        if (totalRecord > 0) {
            accessProviderBoList = this.accessProviderRepository.queryAccessProviders(accessProviderQuery);
            if (!CollectionUtils.isEmpty(accessProviderBoList)) {
                accessProviderBoList.forEach(accessProviderBO -> {
                    List<String> attachList = getAttachList(accessProviderBO.getId());
                    if (!CollectionUtils.isEmpty(attachList)) {
                        accessProviderBO.setAttachUrl(attachList.get(0));
                    }
                });
            }
        }
        return new ListRecordTotalBO<>(accessProviderBoList, totalRecord);
    }


}