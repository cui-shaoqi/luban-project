package com.tarena.tp.luban.demand.admin.bo;

import lombok.Data;

@Data
public class RequestOrderBO {

    private Long id;

    private String requestOrderNo;

    private String orderName;

    private Integer orderCategoryId;

    private String orderCategoryName;

    private Long orderAmount;

    private String userName;

    private String userPhone;

    private Integer areaId;

    private String address;

    private Long serviceTime;

    private Integer status;

    private Integer grabStatus;

    private Integer providerId;

    private String providerName;

    private String createUserName;

    private Long createUserId;

    private Long modifiedUserId;

    private String modifiedUserName;

    private Long gmtModified;

    private Long gmtCreate;
}
