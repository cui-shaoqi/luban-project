package com.tarena.tp.luban.demand.admin.infrastructure.persistence;

import com.tarena.tp.luban.demand.admin.bo.RequestOrderBO;
import com.tarena.tp.luban.demand.admin.dao.RequestOrderDAO;
import com.tarena.tp.luban.demand.admin.infrastructure.persistence.data.converter.RequestOrderConverter;
import com.tarena.tp.luban.demand.admin.protocol.param.RequestOrderParam;
import com.tarena.tp.luban.demand.admin.protocol.query.RequestOrderQuery;
import com.tarena.tp.luban.demand.admin.repository.RequestOrderRepository;
import com.tarena.tp.luban.demand.po.RequestOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RequestOrderRepositoryImpl implements RequestOrderRepository {

    @Autowired
    RequestOrderConverter requestOrderConverter;

    @Autowired
    RequestOrderDAO requestOrderDao;


    @Override
    public RequestOrderBO getRequestOrder(Long requestOrderId) {
        RequestOrder requestOrder = this.requestOrderDao.getEntity(requestOrderId);
        return this.requestOrderConverter.po2bo(requestOrder);
    }

    @Override public List<RequestOrderBO> queryRequestOrders(
            RequestOrderQuery requestOrderQuery) {
        List<RequestOrder> requestOrderList = this.requestOrderDao.queryRequestOrders(this.requestOrderConverter.toDbPagerQuery(requestOrderQuery));
        return this.requestOrderConverter.poList2BoList(requestOrderList);
    }

    @Override public Long getRequestOrderCount(RequestOrderQuery requestOrderQuery) {
        return this.requestOrderDao.countRequestOrder(this.requestOrderConverter.toDbPagerQuery(requestOrderQuery));
    }



}

