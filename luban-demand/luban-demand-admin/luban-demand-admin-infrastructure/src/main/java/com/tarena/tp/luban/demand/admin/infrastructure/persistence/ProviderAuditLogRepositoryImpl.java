package com.tarena.tp.luban.demand.admin.infrastructure.persistence;


import com.tarena.tp.luban.demand.admin.bo.ProviderAuditLogBO;
import com.tarena.tp.luban.demand.admin.dao.ProviderAuditLogDAO;
import com.tarena.tp.luban.demand.admin.infrastructure.persistence.data.converter.ProviderAuditLogConverter;
import com.tarena.tp.luban.demand.admin.protocol.param.ProviderAuditLogParam;
import com.tarena.tp.luban.demand.admin.repository.ProviderAuditLogRepository;
import com.tarena.tp.luban.demand.po.ProviderAuditLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProviderAuditLogRepositoryImpl implements ProviderAuditLogRepository {

    @Autowired
    private ProviderAuditLogConverter providerAuditLogConverter;

    @Autowired
    private ProviderAuditLogDAO providerAuditLogDAO;


    @Override
    public Integer saveProviderAuditLog(ProviderAuditLogParam providerAuditLogParam) {
        ProviderAuditLog providerAuditLog = this.providerAuditLogConverter.param2po(providerAuditLogParam);
        return this.providerAuditLogDAO.insert(providerAuditLog);
    }

    @Override
    public List<ProviderAuditLogBO> getAuditLogByProviderId(Long providerId) {
        List<ProviderAuditLog> providerAuditLog = this.providerAuditLogDAO.getAuditLogByProviderId(providerId);
        return this.providerAuditLogConverter.pos2bos(providerAuditLog);
    }
}

