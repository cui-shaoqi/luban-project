package com.tarena.tp.luban.demand.server.infrastructure.persistence.repository;

import com.tarena.tp.luban.demand.po.RequestOrder;
import com.tarena.tp.luban.demand.server.bo.RequestOrderBO;
import com.tarena.tp.luban.demand.server.dao.RequestOrderDao;
import com.tarena.tp.luban.demand.server.infrastructure.persistence.data.converter.RequestOrderConverter;
import com.tarena.tp.luban.demand.server.protocol.param.RequestOrderParam;
import com.tarena.tp.luban.demand.server.protocol.query.RequestOrderQuery;
import com.tarena.tp.luban.demand.server.repository.RequestOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RequestOrderRepositoryImpl implements RequestOrderRepository {

    @Autowired
    RequestOrderConverter requestOrderConverter;

    @Autowired
    RequestOrderDao requestOrderDao;

    @Override
    public Long save(RequestOrderParam requestOrderParam) {
        RequestOrder requestOrder = this.requestOrderConverter.param2po(requestOrderParam);
        this.requestOrderDao.insert(requestOrder);
        return  requestOrder.getId();
    }

    @Override
    public Integer grabOrder(String requestOrderNo) {
        return this.requestOrderDao.grabOrder(requestOrderNo);
    }

    @Override
    public Integer cancelGrabOrder(String requestOrderNo) {
        return this.requestOrderDao.cancelGrabOrder(requestOrderNo);
    }

    @Override
    public RequestOrderBO getRequestOrder(String requestOrderNo) {
        RequestOrder requestOrder = this.requestOrderDao.getRequestOrderByOrderNo(requestOrderNo);
        return this.requestOrderConverter.po2bo(requestOrder);
    }

    @Override public List<RequestOrderBO> queryRequestOrders(
            RequestOrderQuery requestOrderQuery) {
        List<RequestOrder> requestOrderList = this.requestOrderDao.queryRequestOrders(this.requestOrderConverter.toDbPagerQuery(requestOrderQuery));
        return this.requestOrderConverter.poList2BoList(requestOrderList);
    }

    @Override public Long getRequestOrderCount(RequestOrderQuery requestOrderQuery) {
        return this.requestOrderDao.countRequestOrder(this.requestOrderConverter.toDbPagerQuery(requestOrderQuery));
    }



}

