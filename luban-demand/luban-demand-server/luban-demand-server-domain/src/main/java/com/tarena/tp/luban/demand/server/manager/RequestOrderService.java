/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.demand.server.manager;

import com.tarena.tp.luban.demand.server.bo.AccessProviderBO;
import com.tarena.tp.luban.demand.server.bo.RequestOrderBO;
import com.tarena.tp.luban.demand.server.protocol.param.RequestOrderParam;
import com.tarena.tp.luban.demand.server.protocol.query.RequestOrderQuery;
import com.tarena.tp.luban.demand.server.repository.AccessProviderRepository;
import com.tarena.tp.luban.demand.server.repository.RequestOrderRepository;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class RequestOrderService {

    @Autowired
    private RequestOrderRepository requestOrderRepository;

    @Autowired
    private AccessProviderRepository accessProviderRepository;


    public Long saveRequestOrder(
            RequestOrderParam requestOrderParam) throws BusinessException {
        return this.requestOrderRepository.save(requestOrderParam);
    }

    public Integer grabOrder(String requestOrderId) {
        return this.requestOrderRepository.grabOrder(requestOrderId);
    }

    public Integer cancelGrabOrder(String requestOrderId) {
        return this.requestOrderRepository.cancelGrabOrder(requestOrderId);
    }


    public RequestOrderBO getRequestOrder(String requestOrderNo) throws BusinessException {
        RequestOrderBO requestOrder = requestOrderRepository.getRequestOrder(requestOrderNo);
        if (requestOrder != null) {
            appendViewPrice(requestOrder);
        }
        return requestOrder;
    }

    private void appendViewPrice(RequestOrderBO order) {
        AccessProviderBO accessProvider = accessProviderRepository.getAccessProvider(Long.valueOf(order.getProviderId()));
        if (accessProvider != null) {
            BigDecimal profitScale = accessProvider.getProfitScale();
            Long workerAmount = getWorkerAmount(order.getOrderAmount(), profitScale);
            order.setViewOrderAmount(workerAmount);
            order.setProfitScale(profitScale);
        }
    }

    public ListRecordTotalBO<RequestOrderBO> queryRequestOrder(
            RequestOrderQuery requestOrderQuery) {
        Long totalRecord = this.requestOrderRepository.getRequestOrderCount(requestOrderQuery);
        List<RequestOrderBO> requestOrderBoList = new ArrayList<>();
        if (totalRecord > 0) {
            requestOrderBoList = this.requestOrderRepository.queryRequestOrders(requestOrderQuery);
            //todo 循环查询
            requestOrderBoList.forEach(this::appendViewPrice);
        }
        return new ListRecordTotalBO<>(requestOrderBoList, totalRecord);
    }


    private Long getWorkerAmount(Long price, BigDecimal scale) {
        if (scale != null) {
            return new BigDecimal(price).multiply(new BigDecimal(100).subtract(scale)).divide(new BigDecimal(100)).longValue();
        }
        //默认20
        return new BigDecimal(price).multiply(new BigDecimal(20)).divide(new BigDecimal(100)).longValue();
    }

}
