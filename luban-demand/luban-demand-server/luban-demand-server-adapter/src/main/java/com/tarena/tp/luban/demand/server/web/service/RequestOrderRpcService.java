package com.tarena.tp.luban.demand.server.web.service;

import com.tarena.tp.luban.demand.server.client.RequestOrderApi;
import com.tarena.tp.luban.demand.server.manager.RequestOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class RequestOrderRpcService implements RequestOrderApi {

    @Autowired
    private RequestOrderService requestOrderService;

    @Override
    public Boolean grabOrder(String requestOrderNo) {
        Integer result = requestOrderService.grabOrder(requestOrderNo);
        return result > 0;
    }
}
