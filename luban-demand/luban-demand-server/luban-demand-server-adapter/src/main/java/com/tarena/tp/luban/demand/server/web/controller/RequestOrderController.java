package com.tarena.tp.luban.demand.server.web.controller;

import com.tarena.tp.luban.demand.server.bo.RequestOrderBO;
import com.tarena.tp.luban.demand.server.manager.RequestOrderService;
import com.tarena.tp.luban.demand.server.protocol.param.RequestOrderParam;
import com.tarena.tp.luban.demand.server.protocol.query.RequestOrderQuery;
import com.tarena.tp.luban.demand.server.web.assemble.RequestOrderAssemble;
import com.tarena.tp.luban.demand.server.web.vo.RequestOrderListItemVO;
import com.tarena.tp.luban.demand.server.web.vo.RequestOrderVO;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tedu.inn.protocol.pager.PagerResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(value = "requestOrder", tags = "需求订单接口")
@RestController
@RequestMapping("demand/order")
public class RequestOrderController {

    @Autowired
    private RequestOrderService requestOrderService;

    @Autowired
    private RequestOrderAssemble requestOrderAssemble;

    @PostMapping(value = "search")
    @ResponseBody
    @ApiOperation("需求订单列表")
    public PagerResult<RequestOrderListItemVO> queryDemandOrder(@RequestBody RequestOrderQuery requestOrderQuery) {
        ListRecordTotalBO<RequestOrderBO> accessProviderListTotalRecord =
                this.requestOrderService.queryRequestOrder(requestOrderQuery);
        return this.requestOrderAssemble.assemblePagerResult(accessProviderListTotalRecord, requestOrderQuery);
    }

    @PostMapping(value = "save")
    @ResponseBody
    @ApiOperation("需求单创建")
    public Long saveDemandOrder(@RequestBody RequestOrderParam requestOrderParam) throws BusinessException {
        return this.requestOrderService.saveRequestOrder(requestOrderParam);
    }

    @PostMapping(value = "grab")
    @ResponseBody
    @ApiOperation("抢单")
    public Integer grab(@RequestParam String requestOrderId) {
        return this.requestOrderService.grabOrder(requestOrderId);
    }

    @PostMapping(value = "cancel")
    @ResponseBody
    @ApiOperation("取消抢单")
    public Integer cancelGrab(@RequestParam String requestOrderId) {
        return this.requestOrderService.cancelGrabOrder(requestOrderId);
    }

    @GetMapping(value = "info")
    @ApiOperation("详情")
    public RequestOrderVO getRequestOrder(@RequestParam String requestOrderNo) throws BusinessException {
        RequestOrderBO requestOrderBO = requestOrderService.getRequestOrder(requestOrderNo);
        return this.requestOrderAssemble.assembleBO2RequestOrderVO(requestOrderBO);
    }

}
