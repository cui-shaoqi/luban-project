/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tarena.tp.luban.settle.server.mq.listener;

import javax.annotation.Resource;

import com.tarena.tp.luban.demand.server.manager.RequestOrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RocketMQMessageListener(topic = "${mq.topic.request-order-cancel-topic:request-order-cancel-topic}", consumerGroup = "${rocketmq.producer.group}")
public class OrderListener implements RocketMQListener<String> {

    @Resource
    private RequestOrderService requestOrderService;

    @Override
    public void onMessage(String requestOrderNo) {
        log.info("需求单-取消订单: {}", requestOrderNo);
        requestOrderService.cancelGrabOrder(requestOrderNo);
    }
}
