package com.tarena.tp.luban.demand.po;

import com.tedu.inn.protocol.dao.PO;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Table(name = "request_order")
public class RequestOrder extends PO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "int(11)", nullable = false)
    private Long id;

    @Column(name = "request_order_no", columnDefinition = "varchar(64) DEFAULT '' COMMENT '订单编号'", nullable = false)
    private String requestOrderNo;

    @Column(name = "order_name", columnDefinition = "varchar(64) DEFAULT '' COMMENT '订单名称'", nullable = false)
    private String orderName;

    @Column(name = "order_category_id", columnDefinition = "int(11) DEFAULT 0 COMMENT '订单类型'", nullable = false)
    private Integer orderCategoryId;

    @Column(name = "order_category_name", columnDefinition = "varchar(64) DEFAULT '' COMMENT '订单类型中文'", nullable = false)
    private String orderCategoryName;

    @Column(name = "order_amount", columnDefinition = "int(11) DEFAULT 0 COMMENT '订单金额'", nullable = false)
    private Long orderAmount;

    @Column(name = "user_name", columnDefinition = "varchar(64) DEFAULT '' COMMENT '用户姓名'", nullable = false)
    private String userName;

    @Column(name = "user_phone", columnDefinition = "varchar(16) DEFAULT '' COMMENT '用户电话'", nullable = false)
    private String userPhone;

    @Column(name = "area_id", columnDefinition = "int(11) DEFAULT 0 COMMENT '地址id'", nullable = false)
    private Integer areaId;

    @Column(name = "address", columnDefinition = "varchar(64) DEFAULT '' COMMENT '服务地址'", nullable = false)
    private String address;

    @Column(name = "service_time", columnDefinition = "bigint(11) DEFAULT 0 COMMENT '服务时间'", nullable = false)
    private Long serviceTime;

    @Column(name = "status", columnDefinition = "TINYINT(2) DEFAULT 0 COMMENT '状态'", nullable = false)
    private Integer status;

    @Column(name = "grab_status", columnDefinition = "TINYINT(2) DEFAULT 0 COMMENT '抢单状态'", nullable = false)
    private Integer grabStatus;


    @Column(name = "provider_id", columnDefinition = "int(11) DEFAULT 0 COMMENT '供应商id'", nullable = false)
    private Integer providerId;

}
