package com.tarena.tp.luban.demand.po;

import com.tedu.inn.protocol.dao.PO;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Table(name = "access_provider")
public class AccessProvider extends PO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "int(11)", nullable = false)
    private Long id;

    @Column(name = "provider_name", columnDefinition = "varchar(32) DEFAULT '' COMMENT '供应商名称'", nullable = false)
    private String ProviderName;

    @Column(name = "private_key", columnDefinition = "varchar(512) DEFAULT '' COMMENT '私钥'", nullable = false, updatable = false)
    private String privateKey;

    @Column(name = "public_key", columnDefinition = "varchar(512) DEFAULT '' COMMENT '公钥'", nullable = false, updatable = false)
    private String publicKey;

    @Column(name = "description", columnDefinition = "varchar(512) DEFAULT '' COMMENT '描述'", nullable = false)
    private String description;

    @Column(name = "address", columnDefinition = "varchar(128) DEFAULT '' COMMENT '地址'", nullable = false)
    private String address;

    @Column(name = "principal", columnDefinition = "varchar(32) DEFAULT '' COMMENT '负责人'", nullable = false)
    private String principal;

    @Column(name = "principal_tel", columnDefinition = "varchar(16) DEFAULT '' COMMENT '负责人电话'", nullable = false)
    private String principalTel;

    @Column(name = "profit_scale", columnDefinition = "decimal(20,2) DEFAULT 0 COMMENT '分润比例'", nullable = false)
    private BigDecimal profitScale;

    @Column(name = "status", columnDefinition = "TINYINT(2) DEFAULT 0 COMMENT '状态'", nullable = false)
    private Integer status;

    @Column(name = "audit_status", columnDefinition = "TINYINT(2) DEFAULT 0 COMMENT '审核状态'", nullable = false)
    private Integer auditStatus;


}
