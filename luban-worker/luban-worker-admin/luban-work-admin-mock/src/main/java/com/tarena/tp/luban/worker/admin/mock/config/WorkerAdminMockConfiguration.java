package com.tarena.tp.luban.worker.admin.mock.config;

import com.tarena.tp.attach.server.client.AttachApi;
import com.tarena.tp.attach.server.dto.AttachDTO;
import com.tarena.tp.attach.server.param.AttachUpdateParam;
import com.tarena.tp.attach.server.query.AttachQuery;
import com.tarena.tp.luban.account.server.client.AccountApi;
import com.tarena.tp.luban.account.server.dto.AccountDTO;
import com.tarena.tp.luban.account.server.param.AccountParam;
import com.tarena.tp.luban.account.server.param.PaymentParam;
import com.tarena.tp.luban.worker.admin.mock.properties.WorkerAdminMockProperties;
import java.util.List;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 没有实现账户和附件的实现类时
 */
@Configuration
@EnableConfigurationProperties(WorkerAdminMockProperties.class)
@ConditionalOnProperty(prefix = "luban.worker.admin.mock", name = "mockEnable", havingValue = "true")
public class WorkerAdminMockConfiguration {
    @Bean(name = "accountApi")
    public AccountApi mockAccountApi(){
       AccountApi accountApi=new AccountApi() {
           @Override public Long create(AccountParam param) {
               return null;
           }

           @Override public Long mockPayment(PaymentParam param) {
               return null;
           }

           @Override public AccountDTO getAccountByUserId(Long aLong) {
               return null;
           }
       };
       return accountApi;
    }
    @Bean(name = "attachApi")
    public AttachApi mockAttackApi(){
        AttachApi attachApi=new AttachApi() {
            @Override public List<AttachDTO> getAttachInfoByParam(AttachQuery query) {
                return null;
            }

            @Override public int batchUpdateAttachByIdList(List<AttachUpdateParam> list) {
                return 0;
            }

            @Override public int deleteAttachByBusinessIdAndBusinessType(AttachUpdateParam param) {
                return 0;
            }

            @Override public int deleteAttachById(Long aLong) {
                return 0;
            }

            @Override public int deleteAttachInfoByParam(AttachQuery query) {
                return 0;
            }
        };
        return attachApi;
    }
}
