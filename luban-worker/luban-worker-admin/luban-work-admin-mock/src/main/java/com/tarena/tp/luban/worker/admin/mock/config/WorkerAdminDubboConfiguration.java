package com.tarena.tp.luban.worker.admin.mock.config;

import com.tarena.tp.luban.worker.admin.mock.properties.WorkerAdminMockProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@EnableConfigurationProperties(WorkerAdminMockProperties.class)
@ImportResource({"classpath:applicationContext.xml"})
@ConditionalOnProperty(prefix = "luban.worker.admin.mock", name = "mockEnable", havingValue = "false")
public class WorkerAdminDubboConfiguration {
}
