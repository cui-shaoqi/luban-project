package com.tarena.tp.luban.worker.admin.mock.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
@Data
@ConfigurationProperties(prefix = "luban.worker.admin.mock")
public class WorkerAdminMockProperties {
    private Boolean mockEnable;
}
