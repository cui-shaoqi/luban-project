/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.worker.admin.controller;

import com.tarena.tp.luban.worker.server.common.enums.AuditStatusEnum;
import com.tarena.tp.luban.worker.admin.assemble.WorkerAssemble;
import com.tarena.tp.luban.worker.admin.assemble.WorkerAuditAssemble;
import com.tarena.tp.luban.worker.admin.bo.WorkerBO;
import com.tarena.tp.luban.worker.admin.manager.AuditService;
import com.tarena.tp.luban.worker.admin.manager.WorkerService;
import com.tarena.tp.luban.worker.admin.protocol.param.AuditParam;
import com.tarena.tp.luban.worker.admin.protocol.query.WorkerQuery;
import com.tarena.tp.luban.worker.admin.protocol.vo.AuditDetailVO;
import com.tarena.tp.luban.worker.admin.protocol.vo.WorkerDetailVO;
import com.tarena.tp.luban.worker.admin.protocol.vo.WorkerVO;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tedu.inn.protocol.pager.PagerResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Collections;

@RestController
@RequestMapping("admin/worker")
@Api(value = "worker",tags = "师傅服务")
public class WorkerController {
    @Resource
    private WorkerService workerService;

    @Resource
    private AuditService auditService;
    @Resource
    private WorkerAssemble workerAssemble;

    @Resource
    private WorkerAuditAssemble workerAuditAssemble;


    @PostMapping(value = "search")
    @ResponseBody
    @ApiOperation(value = "师傅列表")
    public PagerResult<WorkerVO> queryWorkers(@RequestBody WorkerQuery workerQuery) {
        workerQuery.setAuditStatus(Collections.singletonList(AuditStatusEnum.APPROVED.getValue()));
        ListRecordTotalBO<WorkerBO> workerListTotalRecord = this.workerService.queryWorker(workerQuery);
        return this.workerAssemble.assemblePagerResult(workerListTotalRecord, workerQuery);
    }

    @PostMapping(value = "aduit")
    @ResponseBody
    @ApiOperation(value = "审核列表")
    public PagerResult<WorkerVO> queryWorkersList(@RequestBody WorkerQuery workerQuery) {
        workerQuery.setAuditStatus(AuditStatusEnum.getUnAuditValue(workerQuery.getAuditStatus()));
        ListRecordTotalBO<WorkerBO> workerListTotalRecord = this.workerService.queryWorker(workerQuery);
        return this.workerAssemble.assemblePagerResult(workerListTotalRecord, workerQuery);
    }


    @GetMapping(value = "get" )
    @ApiOperation(value = "师傅详情")
    @ApiImplicitParam(name = "Id",value = "主键ID",dataType = "Long",required = true)
    public WorkerDetailVO getWorker(@RequestParam Long Id) throws BusinessException {
        WorkerBO workerBo = workerService.getWorker(Id);
        return this.workerAssemble.assembleBO2DetailVO(workerBo);
    }


    @PostMapping("enable" )
    @ApiOperation(value = "师傅启用")
    @ApiImplicitParam(name = "id",value = "师傅 ID",dataType = "Long",required = true)
    public Integer enableWorker(@RequestParam Long id) throws BusinessException {
        return this.workerService.enableWorker(id);
    }

    @PostMapping("disable")
    @ApiOperation(value = "师傅禁用")
    @ApiImplicitParam(name = "id",value = "师傅 ID",dataType = "Long",required = true)
    public Integer disableWorker(@RequestParam Long id) throws BusinessException {
        return this.workerService.disableWorker(id);
    }


    /**
     * 新增审核
     * 入参 id
     */
    @PostMapping("audit/save")
    @ApiOperation(value = "新增审核")
    public Long saveAudit(@RequestBody AuditParam auditParam) throws BusinessException{
        return this.auditService.saveAudit(auditParam);
    }

    /**
     * 审核详情
     */
    @PostMapping(value = "audit/detail")
    @ApiOperation(value = "审核详情")
    public AuditDetailVO workerAudit(@RequestParam Long workerId) throws BusinessException{
        WorkerBO audit = auditService.getAudit(workerId);
        return this.workerAuditAssemble.assembleBO2DetailVO(audit);
    }
}











