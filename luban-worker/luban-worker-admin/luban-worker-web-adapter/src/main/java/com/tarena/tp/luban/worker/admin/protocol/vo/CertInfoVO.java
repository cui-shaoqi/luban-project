package com.tarena.tp.luban.worker.admin.protocol.vo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 师傅详情---认证信息
 */
@Data
public class CertInfoVO {
    /**
     * 认证状态
     */
    @ApiModelProperty("认证状态 0:认证无效 1:认证通过")
    private  Long certStatus;

    @ApiModelProperty("真实姓名")
    private String realName;

    @ApiModelProperty("证件号码")
    private String idCard;
}
