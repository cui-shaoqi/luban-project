package com.tarena.tp.luban.worker.admin.assemble;


import com.tarena.tp.luban.worker.admin.bo.AuditBO;
import com.tarena.tp.luban.worker.admin.bo.WorkerBO;
import com.tarena.tp.luban.worker.admin.protocol.vo.AttachInfoVO;
import com.tarena.tp.luban.worker.admin.protocol.vo.AuditDetailVO;
import com.tarena.tp.luban.worker.admin.protocol.vo.AuditInfoVo;
import com.tarena.tp.luban.worker.admin.protocol.vo.AuditWorkerInfoVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class WorkerAuditAssemble {

    public AuditDetailVO assembleBO2DetailVO(WorkerBO bo){
        AuditDetailVO auditDetailVO = new AuditDetailVO();
        //审核内容
        auditDetailVO.setAuditInfo(assembleAuditInfoVo(bo.getWorkerAuditLogs()));
        //审核意见
        auditDetailVO.setAuditWorkerInfoVO(assembleAuditWorkerInfoVO(bo));
        return auditDetailVO;
    }

    /**
     * 审核内容转换
     */
    public AuditWorkerInfoVO assembleAuditWorkerInfoVO(WorkerBO bo) {
        AuditWorkerInfoVO auditWorkerInfoVO = new AuditWorkerInfoVO();
        BeanUtils.copyProperties(bo, auditWorkerInfoVO);
        return auditWorkerInfoVO;
    }
    /**
     * 基础信息转换
     */
    public AttachInfoVO assembleAttachInfoVO(AuditBO bo) {
        AttachInfoVO attachInfoVO = new AttachInfoVO();
        BeanUtils.copyProperties(bo, attachInfoVO);
        return attachInfoVO;
    }
    /**
     * 审核意见
     */
    public List<AuditInfoVo> assembleAuditInfoVo(List<AuditBO> bos) {
        if (CollectionUtils.isEmpty(bos)) {
            return Collections.emptyList();
        }
        List<AuditInfoVo> result = new ArrayList<>();
        bos.forEach(bo->{
            AuditInfoVo auditInfoVo = new AuditInfoVo();
            BeanUtils.copyProperties(bo, auditInfoVo);
            result.add(auditInfoVo);
        });
        return result;
    }
}
