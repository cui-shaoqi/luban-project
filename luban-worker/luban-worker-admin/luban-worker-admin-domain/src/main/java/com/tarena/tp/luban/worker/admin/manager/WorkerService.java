/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.worker.admin.manager;

import com.tarena.tp.attach.server.client.AttachApi;
import com.tarena.tp.attach.server.dto.AttachDTO;
import com.tarena.tp.attach.server.query.AttachQuery;
import com.tarena.tp.luban.worker.admin.bo.AttachInfoBO;
import com.tarena.tp.luban.worker.admin.bo.WorkerAreaBO;
import com.tarena.tp.luban.worker.admin.bo.WorkerBO;
import com.tarena.tp.luban.worker.admin.bo.WorkerCategoryBO;
import com.tarena.tp.luban.worker.admin.protocol.query.WorkerQuery;
import com.tarena.tp.luban.worker.admin.repository.WorkerAreaRepository;
import com.tarena.tp.luban.worker.admin.repository.WorkerCategoryRepository;
import com.tarena.tp.luban.worker.server.common.config.IdCardConfig;
import com.tarena.tp.luban.worker.server.common.enums.ResultEnum;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tarena.tp.luban.worker.admin.repository.WorkerRepository;
import com.tarena.tp.luban.worker.admin.protocol.param.WorkerParam;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class WorkerService {

    @Autowired
    private WorkerRepository workerRepository;

    @Autowired
    private WorkerAreaRepository workerAreaRepository;

    @Autowired
    private WorkerCategoryRepository workerCategoryRepository;

    @Autowired
    private AttachApi attachApi;

    @Value("${url}")
    private String url;

    @Value("${app.business.type}")
    private Integer businessType;


    public Integer enableWorker(Long workerId) throws BusinessException {
        return this.workerRepository.enable(workerId);
    }

    public Integer disableWorker(Long workerId) throws BusinessException {
        return this.workerRepository.disable(workerId);
    }

    public ListRecordTotalBO<WorkerBO> queryWorker(WorkerQuery workerQuery) {
            Long totalRecord = this.workerRepository.getWorkerCount(workerQuery);
            List<WorkerBO> workerBoList = null;
            if (totalRecord > 0) {
                workerBoList = this.workerRepository.queryWorkers(workerQuery);
                for (WorkerBO workerBO : workerBoList) {
                    appendServiceInfo(workerBO);
                }
            }
            return new ListRecordTotalBO<>(workerBoList, totalRecord);

    }

    public WorkerBO getWorker(Long Id) throws BusinessException {
        WorkerBO worker = workerRepository.getWorker(Id);
        if (worker != null) {
            try{
                //获取图片信息
                List<AttachInfoBO> attachList = getAttachList(worker.getId());
                List<String> collect = attachList.stream().map(AttachInfoBO::getUrl).collect(Collectors.toList());
                worker.setAttachInfo(collect);
            }catch (Exception e){
                throw new BusinessException(ResultEnum.ATTACH_FAILED);
            }
        }
        return worker;
    }

    private void appendServiceInfo(WorkerBO workerBO){
        List<WorkerAreaBO> workerArea = workerAreaRepository.getWorkerArea(workerBO.getUserId());
        if (!CollectionUtils.isEmpty(workerArea)) {
            List<String> collect = workerArea.stream().map(WorkerAreaBO::getAreaDetail).collect(Collectors.toList());
            workerBO.setAreaDetails(collect);
        }
        List<WorkerCategoryBO> workerCategory = workerCategoryRepository.getWorkerCategory(workerBO.getUserId());
        if (!CollectionUtils.isEmpty(workerCategory)) {
            List<String> collect = workerCategory.stream().map(WorkerCategoryBO::getCategoryDetail).collect(Collectors.toList());
            workerBO.setCategoryDetails(collect);
        }
    }

    private List<AttachInfoBO> getAttachList(Long businessId){
        AttachQuery attachQuery = new AttachQuery();
        attachQuery.setBusinessId(businessId.intValue()); //文件 ID
        attachQuery.setBusinessType(businessType); //业务类型
        List<AttachDTO> attachList = attachApi.getAttachInfoByParam(attachQuery);
        List<AttachInfoBO> result = new ArrayList<>();
        if (!CollectionUtils.isEmpty(attachList)) {
            attachList.forEach(attach -> {
                AttachInfoBO attachInfoBO = new AttachInfoBO();
                attachInfoBO.setUrl(url + attach.getFileUuid());
                result.add(attachInfoBO);
            });
        }
        return result;
    }


    public void updateAuditStatus(WorkerParam workerParam) {
        workerRepository.updateAuditStatus(workerParam);
    }

    public void updateCertStatus(WorkerParam workerParam) {
        workerRepository.updateCertStatus(workerParam);
    }
}