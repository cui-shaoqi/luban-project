package com.tarena.tp.luban.worker.admin.infrastructure.persistence;

import com.tarena.tp.luban.worker.admin.bo.AuditBO;
import com.tarena.tp.luban.worker.admin.bo.WorkerBO;
import com.tarena.tp.luban.worker.admin.dao.AuditDAO;
import com.tarena.tp.luban.worker.admin.dao.WorkerDAO;
import com.tarena.tp.luban.worker.admin.infrastructure.persistence.data.converter.AuditConverter;
import com.tarena.tp.luban.worker.admin.infrastructure.persistence.data.converter.WorkerConverter;
import com.tarena.tp.luban.worker.admin.protocol.param.AuditParam;
import com.tarena.tp.luban.worker.admin.repository.AuditLogRepository;
import com.tarena.tp.luban.worker.po.WorkerAuditLog;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class AuditLogRepositoryImpl implements AuditLogRepository {

    @Resource
    private AuditConverter auditConverter;

    @Resource
    private AuditDAO auditDAO;

    /**
     * 新增审核
     * @param auditParam
     * @return
     */
    @Override
    public Long save(AuditParam auditParam) {
        WorkerAuditLog auditLog = this.auditConverter.param2po(auditParam);
        if(auditLog.getId() !=null){
            this.auditDAO.update(auditLog);
            return auditLog.getId();
        }
        this.auditDAO.insert(auditLog);
        return auditLog.getId();
    }

    /**
     * 审核详情
     * @param workerId
     * @return
     */
    @Override
    public List<AuditBO> getAudit(Long workerId) {
        List<WorkerAuditLog> auditLog = auditDAO.getAuditLog(workerId);
        return auditConverter.pos2bos(auditLog);
    }
}
