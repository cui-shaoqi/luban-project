/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.worker.admin.infrastructure.persistence;

import com.tarena.tp.luban.worker.admin.dao.WorkerAreaDAO;
import com.tarena.tp.luban.worker.admin.dao.WorkerCategoryDAO;
import com.tarena.tp.luban.worker.admin.dao.WorkerDAO;
import com.tarena.tp.luban.worker.po.WorkerArea;
import com.tarena.tp.luban.worker.po.WorkerCategory;
import com.tarena.tp.luban.worker.server.common.enums.ResultEnum;
import com.tedu.inn.commons.utils.Asserts;

import com.tedu.inn.protocol.enums.StatusRecord;
import com.tedu.inn.protocol.dao.StatusCriteria;
import com.tarena.tp.luban.worker.admin.infrastructure.persistence.data.converter.WorkerConverter;
import com.tarena.tp.luban.worker.po.Worker;
import com.tarena.tp.luban.worker.admin.bo.WorkerBO;
import com.tarena.tp.luban.worker.admin.protocol.param.WorkerParam;
import com.tarena.tp.luban.worker.admin.repository.WorkerRepository;
import com.tarena.tp.luban.worker.admin.protocol.query.WorkerQuery;

import java.util.List;
import java.util.stream.Collectors;

import com.tedu.inn.protocol.exception.BusinessException;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;

@Component
public class WorkerRepositoryImpl implements WorkerRepository {
    @Resource
    private WorkerConverter workerConverter;

    @Resource
    private WorkerDAO workerDao;

    @Resource
    private WorkerAreaDAO workerAreaDAO;

    @Resource
    private WorkerCategoryDAO workerCategoryDAO;


    @Override public Integer disable(Long Id) {
        String  workerId =String.valueOf(Id);
        StatusCriteria statusCriteria = new StatusCriteria(workerId, StatusRecord.DISABLE.getStatus());
        this.workerConverter.convertStatus(statusCriteria);
        return this.workerDao.changeStatus(statusCriteria);
    }

    @Override public Integer enable(Long Id) {
        String workerId=String.valueOf(Id);
        StatusCriteria statusCriteria = new StatusCriteria(workerId, StatusRecord.ENABLE.getStatus());
        this.workerConverter.convertStatus(statusCriteria);
        return this.workerDao.changeStatus(statusCriteria);
    }

    @Override
    public void updateAuditStatus(WorkerParam workerParam) {
        Worker worker = this.workerConverter.param2po(workerParam);
        this.workerDao.updateWorkerAuditStatus(worker);
    }

    @Override
    public void updateCertStatus(WorkerParam workerParam) {
        Worker worker = this.workerConverter.param2po(workerParam);
        this.workerDao.updateWorkerCertStatus(worker);
    }


    /**
     * 根据 ID 查询 worker 信息
     * 再根据 workerID 查询区域详情
     * @param
     * @return
     */
    @Override public WorkerBO getWorker(Long Id) throws BusinessException {
        //根据 ID 查询 worker 信息
        Worker worker = this.workerDao.getEntity(Id);
        Asserts.isTrue(worker == null,new BusinessException(ResultEnum.WORKER_NOT_FOUND));
        //从查询出的师傅区域和师傅品类中获取详情存储到 WorkerBO 中
        WorkerBO workerBO = workerConverter.po2bo(worker);
        //再根据 user_Id 去 worker_area 表中查询区域详情
        List<WorkerArea> workerAreas = this.workerAreaDAO.getWorkerAreaByUserId(worker.getUserId());
        if (!CollectionUtils.isEmpty(workerAreas)) {
            List<String> collect = workerAreas.stream().map(WorkerArea::getAreaDetail).collect(Collectors.toList());
            workerBO.setAreaDetails(collect);
        }
        //再根据 user_Id 去 worker_category 表中查询品类详情
        List<WorkerCategory> workerCategory = this.workerCategoryDAO.getCategoryByUserId(worker.getUserId());
        if (!CollectionUtils.isEmpty(workerCategory)) {
            List<String> collect = workerCategory.stream().map(WorkerCategory::getCategoryDetail).collect(Collectors.toList());
            workerBO.setCategoryDetails(collect);
        }
        return workerBO;
    }

    @Override public List<WorkerBO> queryWorkers(
        WorkerQuery workerQuery) {
        List<Worker> workerList = this.workerDao.queryWorkers(this.workerConverter.toDbPagerQuery(workerQuery));
        return this.workerConverter.poList2BoList(workerList);
    }

    @Override public Long getWorkerCount(WorkerQuery workerQuery) {
        return this.workerDao.countWorker(this.workerConverter.toDbPagerQuery(workerQuery));
    }
}