package com.tarena.tp.luban.worker.server.common.enums;

import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

@Getter
public enum IdCardEnum {

    FRONT(1, "正面"),

    BACK(2,"背面");

    IdCardEnum(Integer value, String message) {
        this.value = value;
        this.message = message;
    }

    public static boolean IdCardEnum(Integer value) {
        return Arrays.stream(PictureTypeEnum.values()).anyMatch(o -> Objects.equals(o.getValue(), value));
    }

    private Integer value;

    private String message;
}
