package com.tarena.tp.luban.worker.server.common.enums;


import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;

/**
 * 审核状态枚举类
 */
public enum AuditStatusEnum {
    REJECTED(0, "驳回"),
    APPROVED(1, "通过"),
    UNAUDITED(2, "未审核"),
    UNCREATED(3, "未入驻");

    private Integer value;
    private String description;

    AuditStatusEnum(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public static List<Integer> getUnAuditValue(List<Integer> status) {
        if (!CollectionUtils.isEmpty(status)) {
            return status;
        }
        return Arrays.asList(REJECTED.value, UNAUDITED.value);
    }

    public Integer getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }
}

