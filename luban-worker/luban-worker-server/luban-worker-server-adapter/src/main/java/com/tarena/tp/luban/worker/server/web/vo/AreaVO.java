package com.tarena.tp.luban.worker.server.web.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AreaVO {


    @ApiModelProperty(value = "区域列表 ID")
    Long areaId;

    @ApiModelProperty(value = "区域详情")
    String areaDetail;
}














