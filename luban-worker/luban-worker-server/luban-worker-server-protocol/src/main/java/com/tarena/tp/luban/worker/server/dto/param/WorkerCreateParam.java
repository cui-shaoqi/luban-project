package com.tarena.tp.luban.worker.server.dto.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WorkerCreateParam {
    /**
     * 主键 id
     */
    @ApiModelProperty("用户 ID")
    Long userId;

    /**
     * 真实姓名
     */
    @ApiModelProperty("真实姓名")
    String realName;

    /**
     * 手机号
     */
    @ApiModelProperty("手机号")
    String phone;

    /**
     * 身份证号
     */
    @ApiModelProperty("身份证号")
    String idCard;

    /**
     * 身份证图片地址
     */
    @ApiModelProperty("图片地址")
    List<IdCardParam> attachList;

    /**
     * 区域 id
     */
    @ApiModelProperty("区域信息")
    List<WorkerAreaParam>  workerAreaParams;

    /**
     * 区域 id
     */
    @ApiModelProperty("品类信息")
    List<WorkerCategoryParam>  workerCategoryParams;

}














