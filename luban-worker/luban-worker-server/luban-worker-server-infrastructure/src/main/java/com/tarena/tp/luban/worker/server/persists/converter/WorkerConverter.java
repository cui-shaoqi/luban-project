package com.tarena.tp.luban.worker.server.persists.converter;

import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.sdk.context.SecurityContext;
import com.tarena.tp.luban.worker.server.bo.WorkerBO;
import com.tarena.tp.luban.worker.server.common.enums.AccountStatusEnum;
import com.tarena.tp.luban.worker.server.common.enums.AuditStatusEnum;
import com.tarena.tp.luban.worker.server.common.enums.StatusEnum;
import com.tarena.tp.luban.worker.server.dto.param.WorkerCreateParam;
import com.tarena.tp.luban.worker.server.dto.param.WorkerParam;
import com.tarena.tp.luban.worker.po.Worker;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class WorkerConverter {

    /**
     * Param---> PO
     * WorkerCreateParam--->Worker
     */
    public Worker param2po(WorkerCreateParam param){
        Worker worker = new Worker();
        if (param==null){
            return new Worker();
        }
        BeanUtils.copyProperties(param, worker);
        LoginUser loginUser = SecurityContext.getLoginToken();
        worker.setGmtCreate(System.currentTimeMillis());
        worker.setGmtModified(System.currentTimeMillis());
        worker.setCreateUserId(loginUser != null ? loginUser.getUserId() : 0);
        worker.setModifiedUserId(loginUser != null ? loginUser.getUserId() : 0);
        worker.setCreateUserName(loginUser != null ? loginUser.getUserName() : "mock");
        worker.setModifiedUserName(loginUser != null ? loginUser.getUserName() : "mock");
        //默认是正常的
        worker.setStatus(StatusEnum.NORMAL.getValue());
        //默认未审核
        worker.setAuditStatus(AuditStatusEnum.UNAUDITED.getValue());
        //默认未启用
        worker.setCertStatus(AccountStatusEnum.DISABLE.getValue());
        return worker;
    }


    public Worker param2po(WorkerParam workerParam){
        Worker worker = new Worker();
        if (workerParam==null){
            return new Worker();
        }
        BeanUtils.copyProperties(workerParam, worker);
        LoginUser loginUser = SecurityContext.getLoginToken();
        worker.setGmtCreate(System.currentTimeMillis());
        worker.setGmtModified(worker.getGmtCreate());
        worker.setCreateUserId(loginUser.getUserId());
        worker.setModifiedUserId(loginUser.getUserId());
        worker.setStatus(1);
        worker.setCreateUserName(loginUser.getUserName());
        worker.setModifiedUserName(loginUser.getUserName());
        return worker;
    }

    public WorkerBO po2bo(Worker worker) {
        WorkerBO workerBO = new WorkerBO();
        if (worker == null) {
            return workerBO;
        }
        BeanUtils.copyProperties(worker, workerBO);
        return workerBO;
    }
}
