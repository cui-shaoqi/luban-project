/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.worker.server.persists.repository;

import com.tarena.tp.luban.worker.server.bo.WorkerAreaBO;
import com.tarena.tp.luban.worker.server.dao.WorkerAreaDAO;
import com.tarena.tp.luban.worker.server.dto.param.WorkerAreaParam;
import com.tarena.tp.luban.worker.server.persists.converter.WorkerAreaConverter;
import com.tarena.tp.luban.worker.po.WorkerArea;
import com.tarena.tp.luban.worker.server.repository.WorkerAreaRepository;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class WorkerAreaRepositoryImpl implements WorkerAreaRepository {
    @Resource
    private WorkerAreaConverter workerAreaConverter;

   @Resource
    private WorkerAreaDAO workerAreaDao;

    @Override public Long saveWorkerArea(WorkerAreaParam workerAreaParam) {
        WorkerArea workerArea = this.workerAreaConverter.param2po(workerAreaParam);
        if (workerArea.getId() != null) {
            this.workerAreaDao.update(workerArea);
            return workerArea.getId();
        }
        this.workerAreaDao.insert(workerArea);
        return workerArea.getId();
    }

    @Override
    public List<WorkerAreaBO> getWorkerAreaByWorkerId(Long workerId) {
        List<WorkerArea> workerAreaList = this.workerAreaDao.getAreaByWorkerId(workerId);
        return this.workerAreaConverter.poList2BoList(workerAreaList);
    }

    @Override
    public void deleteByUserId(Long userId) {
        workerAreaDao.delete(userId);
    }

}